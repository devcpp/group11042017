package com.kkrasylnykov.l20_filesandpermisionexamples.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

import java.io.File;
import java.security.cert.PKIXRevocationChecker;

public class ViewImageActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        String strPath = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            strPath = bundle.getString(KEY_FILE_PATH, "");
        }

        if (strPath.isEmpty()){
            finish();
        }

        File imgFile = new  File(strPath);

        if(imgFile.exists()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;

            int nScale = options.outWidth / width;
            if (nScale<1){
                nScale = 1;
            }

            options = new BitmapFactory.Options();
            options.inSampleSize = nScale;

            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            ImageView imageView = (ImageView) findViewById(R.id.ImageView);

            imageView.setImageBitmap(bitmap);

            Log.d("devcpp ","bitmap.getWidth -> " + bitmap.getWidth());
        } else {
            finish();
        }
    }
}
