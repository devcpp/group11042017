package com.kkrasylnykov.l20_filesandpermisionexamples.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;
import com.kkrasylnykov.l20_filesandpermisionexamples.adapters.FileAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FileAdapter.OnClickItemListener{

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 1100;

    FileAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        RecyclerView.LayoutManager  layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FileAdapter();
        adapter.setOnClickItemListener(this);
        recyclerView.setAdapter(adapter);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            loadFileData();
        }
    }

    private void loadFileData(){
        ArrayList<String> arrData = getAllFilesPath(Environment.getExternalStorageDirectory());
        adapter.setData(arrData);
        for (String strPath:arrData){
            Log.d("devcpp","strPath -> " + strPath);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED){
                loadFileData();
            } else {
                Toast.makeText(MainActivity.this, "Need permission!!!!!", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private ArrayList<String> getAllFilesPath(File file){
        ArrayList<String> arrResult = new ArrayList<>();
        if (file!=null && file.exists()){
            if (file.isFile()) {
                if (file.getName().endsWith(".png") ||
                        file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".jpeg")){
                    arrResult.add(file.getAbsolutePath());
                }

            } else if (file.isDirectory()){
                File[] arrFiles = file.listFiles();
                if (arrFiles!=null){
                    for (File curFile:arrFiles){
                        arrResult.addAll(getAllFilesPath(curFile));
                    }
                }

            }
        }
        return arrResult;
    }

    @Override
    public void onClickItem(String str) {
        Intent intent = new Intent(this, ViewImageActivity.class);
        intent.putExtra(ViewImageActivity.KEY_FILE_PATH, str);
        startActivity(intent);
    }
}
