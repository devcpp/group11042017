package com.kkrasylnykov.l32_volleyexmple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text);

        String url ="http://xutpuk.pp.ua/api/users/123.json";

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mTextView.setText(response);
                Gson gson = new Gson();
                UserInfo userInfo = gson.fromJson(response, UserInfo.class);
                Log.d("devcpp", "userInfo.getServerId -> " + userInfo.getServerId());
                Log.d("devcpp", "userInfo.getName -> " + userInfo.getName());
                Log.d("devcpp", "userInfo.getSname -> " + userInfo.getSname());

                userInfo.setServerId(300);
                userInfo.setName("TEST128");
                userInfo.setSname("__TEST128");

                String json = gson.toJson(userInfo);

                Log.d("devcpp", "json -> " + json);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("error: " + error.toString());
            }
        });

        queue.add(stringRequest);
    }
}
