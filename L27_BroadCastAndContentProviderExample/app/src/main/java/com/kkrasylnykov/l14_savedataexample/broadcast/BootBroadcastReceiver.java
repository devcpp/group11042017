package com.kkrasylnykov.l14_savedataexample.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kkrasylnykov.l14_savedataexample.service.BootService;

public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent bootIntent = new Intent(context, BootService.class);
        context.startService(bootIntent);
    }
}
