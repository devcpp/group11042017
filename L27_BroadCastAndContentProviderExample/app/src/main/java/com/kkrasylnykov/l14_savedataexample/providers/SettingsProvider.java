package com.kkrasylnykov.l14_savedataexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class SettingsProvider extends ContentProvider {

    public static final String DATA = "DATA";

    private static final String KEY_BOOLEAN_IS_SHOW_TC = "KEY_BOOLEAN_IS_SHOW_TC";
    private static final String KEY_LONG_LAST_USER_UPDATE = "KEY_LONG_LAST_USER_UPDATE";

    private static final String AUTHORITY = "com.kkrasylnykov.l14_savedataexample.providers.SettingsProvider";

    public static final String SHOW_TC          = "show_tc";
    public static final String LAST_UPDATE      = "last_update";

    public static final Uri SHOW_TC_URI    = Uri.parse("content://" + AUTHORITY + "/" + SHOW_TC);
    public static final Uri LAST_UPDATE_URI    = Uri.parse("content://" + AUTHORITY + "/" + LAST_UPDATE);

    private static final int URI_SHOW_TC            = 1;
    private static final int URI_LAST_UPDATE        = 2;

    private static final UriMatcher uriMatcher;

    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SHOW_TC, URI_SHOW_TC);
        uriMatcher.addURI(AUTHORITY, LAST_UPDATE, URI_LAST_UPDATE);
    }

    private SharedPreferences m_SharedPreferences;

    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String strResult = "";
        switch (uriMatcher.match(uri)){
            case URI_SHOW_TC:
                strResult = KEY_BOOLEAN_IS_SHOW_TC;
                break;
            case URI_LAST_UPDATE:
                strResult = KEY_LONG_LAST_USER_UPDATE;
                break;
        }
        return strResult;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Object data = null;
        switch (uriMatcher.match(uri)){
            case URI_SHOW_TC:
                data = m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_SHOW_TC, true)?1:0;
                break;
            case URI_LAST_UPDATE:
                data = m_SharedPreferences.getLong(KEY_LONG_LAST_USER_UPDATE, -1);
                break;
        }
        MatrixCursor cursor = new MatrixCursor(new String[]{DATA}, 1);
        cursor.addRow(new Object[]{data});
        return cursor;
    }



    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        switch (uriMatcher.match(uri)){
            case URI_SHOW_TC:
                editor.putBoolean(KEY_BOOLEAN_IS_SHOW_TC, values.getAsBoolean(DATA));
                break;
            case URI_LAST_UPDATE:
                editor.putLong(KEY_LONG_LAST_USER_UPDATE, values.getAsLong(DATA));
                break;
        }
        editor.commit();
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
