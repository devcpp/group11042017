package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.providers.SettingsProvider;

public class AppSettings {

    Context context;

    public AppSettings(Context context){
        this.context = context;
    }

    public void setIsShowTC(boolean bIsShowTC){
        ContentValues values = new ContentValues();
        values.put(SettingsProvider.DATA, bIsShowTC);
        context.getContentResolver().insert(SettingsProvider.SHOW_TC_URI,values);
    }

    public boolean isShowTC(){
        boolean bData = false;
        Cursor cursor = context.getContentResolver().query(SettingsProvider.SHOW_TC_URI,null,null,null,null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                bData = cursor.getInt(0)==1;
            }
            cursor.close();
        }
        return bData;
    }

    public void setLastUserUpdate(long nLastUserUpdate){
        ContentValues values = new ContentValues();
        values.put(SettingsProvider.DATA, nLastUserUpdate);
        context.getContentResolver().insert(SettingsProvider.LAST_UPDATE_URI,values);
    }

    public long getLastUserUpdate(){
        long nData = -1;
        Cursor cursor = context.getContentResolver().query(SettingsProvider.LAST_UPDATE_URI,null,null,null,null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                nData = cursor.getLong(0);
            }
            cursor.close();
        }
        return nData;
    }
}
