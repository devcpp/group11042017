package com.kkrasylnykov.l20_filesandpermisionexamples.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

import java.io.File;

public class ImageViewFragment extends Fragment {

    public static final String KEY_PATH = "KEY_PATH";
    private static final String KEY_DATA = "KEY_DATA";
    Thread thread;

    Handler handler;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_view, container, false);
        String strPath = "";

        final TextView infoTextView = (TextView) view.findViewById(R.id.infoTextView);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if (msg==null || msg.getData()==null){
                    return;
                }
                String strData = msg.getData().getString(KEY_DATA);
                infoTextView.setText(strData);
            }
        };

        Bundle bundle = getArguments();
        if (bundle!=null){
            strPath = bundle.getString(KEY_PATH, "");
        }

        if (strPath==null || strPath.isEmpty()){
            getActivity().finish();
        }

        File imgFile = new  File(strPath);

        if(imgFile.exists()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;

            int nScale = options.outWidth / width;
            if (nScale<1){
                nScale = 1;
            }

            options = new BitmapFactory.Options();
            options.inSampleSize = nScale;

            final Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            ImageView imageView = (ImageView) view.findViewById(R.id.ImageViewFragment);

            imageView.setImageBitmap(bitmap);

            //Log.d("devcpp ","bitmap.getWidth -> " + bitmap.getWidth());

            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    int nCountRed = 0;
                    int nCountBlue = 0;
                    int nCountGreen = 0;
                    int nCountSpecial = 0;
                    int nCountTransparent = 0;
                    int nAllPixels = bitmap.getWidth() * bitmap.getHeight();
                    int nCurPixelCount = 0;
                    int oldProcent = 0;
                    for (int x=0; x<bitmap.getWidth(); x++){
                        for (int y=0; y<bitmap.getHeight(); y++){
                            if (thread.isInterrupted()){
                                Log.d("devcpp","isInterrupted");
                                return;
                            }
                            nCurPixelCount++;
                            int procent = (int) (((float)nCurPixelCount*100)/nAllPixels);
                            if (procent>oldProcent){
                                oldProcent = procent;
                                if(handler!=null){
                                    String strProcent = " -> " + procent + " %";
                                    Message msg = new Message();
                                    Bundle dataBundle = new Bundle();
                                    dataBundle.putString(KEY_DATA, strProcent);
                                    msg.setData(dataBundle);
                                    handler.sendMessage(msg);
                                }
                            }

                            if (bitmap.getPixel(x, y) == Color.rgb(256,0,0)){
                                nCountRed ++;
                            }
                            if (bitmap.getPixel(x, y) == Color.rgb(0,256,0)){
                                nCountGreen ++;
                            }
                            if (bitmap.getPixel(x, y) == Color.rgb(0,0,256)){
                                nCountBlue ++;
                            }
                            if (bitmap.getPixel(x, y) == Color.rgb(256,0,256)){
                                nCountSpecial ++;
                            }
                            if (Color.alpha(bitmap.getPixel(x, y)) <20){
                                nCountTransparent ++;
                            }
                        }
                    }

                    final String strData  = "nCountRed -> " + nCountRed +
                            "\nnCountGreen -> " + nCountGreen +
                            "\nnCountBlue -> " + nCountBlue+
                            "\nnCountTransparent -> " + nCountTransparent+
                            "\nnCountSpecial -> " + nCountSpecial;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("devcpp","runOnUiThread");
                            infoTextView.setText(strData);
                        }
                    });

                }
            });
            thread.start();
            Log.d("devcpp","thread.start -> " + Thread.getAllStackTraces().keySet().size());

        } else {
            getActivity().finish();
        }
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (thread!=null && thread.isAlive()){
            thread.interrupt();
        }
    }
}
