package com.kkrasylnykov.l20_filesandpermisionexamples.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

import java.util.ArrayList;

public class FileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<String> arrData;
    OnClickItemListener listener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InfoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file_name, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        InfoViewHolder infoViewHolder = (InfoViewHolder)holder;
        infoViewHolder.fileName.setText(arrData.get(position));
        infoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClickItem(arrData,position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrData!=null?arrData.size():0;
    }

    private class InfoViewHolder extends RecyclerView.ViewHolder{

        private TextView fileName;

        public InfoViewHolder(View itemView) {
            super(itemView);
            fileName = (TextView) itemView.findViewById(R.id.fileNameTextViewItem);
        }
    }

    public void setData(ArrayList<String> arrData){
        this.arrData = arrData;
        notifyDataSetChanged();
    }

    public void setOnClickItemListener(OnClickItemListener listener) {
        this.listener = listener;
    }

    public interface OnClickItemListener {
        void onClickItem(ArrayList<String> arrData, int position);
    }
}
