package com.kkrasylnykov.l20_filesandpermisionexamples.adapters;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l20_filesandpermisionexamples.fragments.ImageViewFragment;

import java.util.ArrayList;

public class ImageViewPagerFragmentAdapter extends FragmentPagerAdapter {

    ArrayList<String> arrData;

    public ImageViewPagerFragmentAdapter(FragmentManager fm, ArrayList<String> arrData) {
        super(fm);
        this.arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        ImageViewFragment fragment = new ImageViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ImageViewFragment.KEY_PATH, arrData.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return arrData.size();
    }
}
