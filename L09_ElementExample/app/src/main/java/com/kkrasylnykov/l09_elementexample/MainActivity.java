package com.kkrasylnykov.l09_elementexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView mResultTextView;
    Button mYesButton;
    Button mNoButton;

    LinearLayout mLinearLayout;
    EditText mNameEditText;

    int mCount = 0;

//    View.OnClickListener onClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultTextView = (TextView) findViewById(R.id.resultTestTextViewMainActivity);

        mYesButton = (Button) findViewById(R.id.yesButtonMainActivity);
        mYesButton.setOnClickListener(this);
        mNoButton = (Button) findViewById(R.id.noButtonMainActivity);
        mNoButton.setOnClickListener(this);

        mLinearLayout = (LinearLayout) findViewById(R.id.inputLinearLayoutMainActivity);

        mNameEditText = (EditText) findViewById(R.id.nameEditTextMainActivity);
//        mYesButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//        View.OnClickListener onClick = this;
    }

    @Override
    public void onClick(View v) {
        v.setOnClickListener(null);
        String strName = mNameEditText.getText().toString();
        if (strName.trim().isEmpty()){
            Toast.makeText(this,"Вы не ввели имя!",Toast.LENGTH_SHORT).show();
            v.setOnClickListener(this);
            return;
        }

        mLinearLayout.setVisibility(View.GONE);
        mResultTextView.setVisibility(View.VISIBLE);
        switch (v.getId()){
            case R.id.yesButtonMainActivity:
                mResultTextView.setText("Поздравляем, " + strName + "! Вы ответили правильно!");
                break;
            case R.id.noButtonMainActivity:
                mResultTextView.setText(strName + "! Вы ответили не правильно!");
                break;
        }
        v.setOnClickListener(this);
    }
}
