package com.kkrasylnykov.l23_syncexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static int COUNT = 5000000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bank bank = new Bank();
                Bank.init();
                Bank.getCash();

                Thread threadAdd = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<COUNT; i++){
                            bank.add();
                        }
                        bank.getCash();
                    }
                });
                Thread threadGet = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<COUNT; i++){
                            bank.get();
                        }
                        bank.getCash();
                    }
                });

                threadAdd.start();
                threadGet.start();

            }
        });
    }

    public static class Bank {
        static long cash = 1000;

        static void init(){
            synchronized(Bank.class){
                cash = 1000;
            }
        }

        static void add(){
            synchronized(Bank.class) {
                cash++;
            }

        }

        void get(){
            synchronized(Bank.class) {
                cash--;
            }

        }

        static void getCash(){
            Log.d("devcpp","cash -> " + cash);
        }
    }
}
