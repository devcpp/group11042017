package l02_recexample;

import java.util.Scanner;

public class L02_RecExample {
    
    public static long pow(int val, int pow){
        long nResult = 0;
        if (pow==0){
            nResult = 1;
        } else {
            nResult = val * pow(val, pow - 1);
        }
        return nResult;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        
        long nResult = pow(m,n);
        
        System.out.println("nResult = " + nResult);
    }
    
}
