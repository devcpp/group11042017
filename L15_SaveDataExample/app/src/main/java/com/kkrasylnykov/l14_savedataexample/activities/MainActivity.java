package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_TC_ACTIVITY = 100;

    private LinearLayout mContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        DBHelper dbHelper = new DBHelper(this);

        if (appSettings.isShowTC()){
            Intent intent = new Intent(this, TCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC_ACTIVITY);

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values;
            for (int i=0;i<5000; i++){
                values = new ContentValues();
                values.put(DBConstants.FIELD_NAME, "Kos" + i);
                values.put(DBConstants.FIELD_SNAME, "Kras" + i);
                values.put(DBConstants.FIELD_PHONE, "095" + i);
                values.put(DBConstants.FIELD_ADDRESS, "Devichia, " + i);
                db.insert(DBConstants.TABLE_NAME, null, values);
            }
            db.close();
        }

        mContainerLinearLayout = (LinearLayout) findViewById(R.id.containerLinearLayoutMainActivity);

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mContainerLinearLayout.removeAllViews();
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    do{
                        long nId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_ID));
                        String strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_PHONE));
                        String strAddress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_ADDRESS));
                        TextView itemTextView = new TextView(this);
                        itemTextView.setText(strName + " " + strSName + "\n" + strPhone + "\n" + strAddress);
                        itemTextView.setLayoutParams(params);
                        itemTextView.setTag(nId);
                        itemTextView.setOnClickListener(this);
                        mContainerLinearLayout.addView(itemTextView);
//                        Log.d("devcpp","nId -> " + nId);
//                        Log.d("devcpp","strName -> " + strName);
//                        Log.d("devcpp","strSName -> " + strSName);
//                        Log.d("devcpp","strPhone -> " + strPhone);
//                        Log.d("devcpp","strAddres -> " + strAddress);
                        //TODO Show data to activity
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_TC_ACTIVITY){
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isShowTC()){
                finish();
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getTag()!=null){
            long nId = (long) v.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.KEY_ID, nId);
            startActivity(intent);
        } else if (v.getId() == R.id.addButtonMainActivity){
            Intent intent = new Intent(this, EditActivity.class);
            startActivity(intent);
        }
    }
}
