package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String KEY_ID = "KEY_ID";

    private EditText mNameEditText;
    private EditText mSNameEditText;
    private EditText mPhoneEditText;
    private EditText mAddressEditText;
    private long nId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mNameEditText = (EditText) findViewById(R.id.nameEditTextEditActivity);
        mSNameEditText = (EditText) findViewById(R.id.snameEditTextEditActivity);
        mPhoneEditText = (EditText) findViewById(R.id.phoneEditTextEditActivity);
        mAddressEditText = (EditText) findViewById(R.id.addrEditTextEditActivity);

        Button btnAdd = (Button) findViewById(R.id.addButtonEditActivity);
        btnAdd.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(KEY_ID,-1);
            }
        }

        if (nId!=-1){
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.removeButtonEditActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String strSelect = DBConstants.FIELD_ID + "=?";
            String[] arrArgs = {Long.toString(nId)};
            Cursor cursor = db.query(DBConstants.TABLE_NAME, null, strSelect, arrArgs,
                    null, null, null);
            if (cursor!=null){
                try{
                    if (cursor.moveToFirst()){
                        String strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_PHONE));
                        String strAddress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_ADDRESS));
                        mNameEditText.setText(strName);
                        mSNameEditText.setText(strSName);
                        mPhoneEditText.setText(strPhone);
                        mAddressEditText.setText(strAddress);
                    }
                } finally {
                    cursor.close();
                }
            }
            db.close();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.addButtonEditActivity){
            String strName = mNameEditText.getText().toString().trim();
            String strSName = mSNameEditText.getText().toString().trim();
            String strPhone = mPhoneEditText.getText().toString().trim();
            String strAddress = mAddressEditText.getText().toString().trim();

            if (strName.isEmpty()){
                Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
                return;
            }

            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DBConstants.FIELD_NAME, strName);
            values.put(DBConstants.FIELD_SNAME, strSName);
            values.put(DBConstants.FIELD_PHONE, strPhone);
            values.put(DBConstants.FIELD_ADDRESS, strAddress);

            if (nId!=-1){
                String strSelect = DBConstants.FIELD_ID + "=?";
                String[] arrArgs = {Long.toString(nId)};
                db.update(DBConstants.TABLE_NAME,values,strSelect,arrArgs);
                db.close();
                finish();
            } else {
                db.insert(DBConstants.TABLE_NAME, null, values);
            }


            mNameEditText.setText("");
            mNameEditText.requestFocus();
            mSNameEditText.setText("");
            mPhoneEditText.setText("");
            mAddressEditText.setText("");

            db.close();
        } else if (v.getId()==R.id.removeButtonEditActivity) {
            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String strSelect = DBConstants.FIELD_ID + "=?";
            String[] arrArgs = {Long.toString(nId)};
            db.delete(DBConstants.TABLE_NAME, strSelect, arrArgs);
            db.close();
            finish();
        }
    }
}
