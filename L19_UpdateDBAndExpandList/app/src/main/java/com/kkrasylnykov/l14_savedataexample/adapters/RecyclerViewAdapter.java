package com.kkrasylnykov.l14_savedataexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.model.BaseEntity;
import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM_USER_INFO = 0;
    private static final int TYPE_ITEM_PHONE_INFO = 3;
    private static final int TYPE_ITEM_TOP = 1;
    private static final int TYPE_ITEM_BOTTOM = 2;

    private ArrayList<UserInfo> mArrData;
    private ArrayList<BaseEntity> mArrShow = new ArrayList<>();

    private OnClickUserInfoItem onClickUserInfoItem = null;

    public RecyclerViewAdapter(ArrayList<UserInfo> arrData){
        mArrData = arrData;
        mArrShow.clear();
        mArrShow.addAll(mArrData);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        if (viewType == TYPE_ITEM_USER_INFO){
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_USER_INFO");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
            holder = new UserInfoViewHolder(view);
        } else if (viewType == TYPE_ITEM_TOP) {
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_TOP");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top, parent, false);
            holder = new TopViewHolder(view);
        } else if (viewType == TYPE_ITEM_BOTTOM) {
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_BOTTOM");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom, parent, false);
            holder = new BottomViewHolder(view);
        } else if (viewType == TYPE_ITEM_PHONE_INFO) {
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_PHONE_INFO");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_phone_info, parent, false);
            holder = new PhoneInfoViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int nType = getItemViewType(position);
        if (nType == TYPE_ITEM_USER_INFO){
            UserInfoViewHolder userInfoViewHolder = (UserInfoViewHolder) holder;
            final UserInfo userInfo = (UserInfo) mArrShow.get(position-1);
            userInfoViewHolder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
            userInfoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int nPosition = mArrShow.indexOf(userInfo)+1;
                    if (userInfo.isExpand()){
                        mArrShow.removeAll(userInfo.getPhones());
                        notifyItemRangeRemoved(nPosition+1,userInfo.getPhones().size());
                        //notifyDataSetChanged();
                    } else {
                        mArrShow.addAll(nPosition, userInfo.getPhones());
                        notifyItemRangeInserted(nPosition+1, userInfo.getPhones().size());
                    }
                    userInfo.changeExpand();
                    //
                }
            });
            /*ArrayList<PhoneInfo> arrPhones = userInfo.getPhones();
            String strPhones = "Not phones";
            if (arrPhones!=null && arrPhones.size()>0){
                strPhones = "";
                for(PhoneInfo phoneInfo:arrPhones){
                    strPhones += phoneInfo.getPhone() + "\n";
                }

            }
            userInfoViewHolder.phoneTextView.setText(strPhones);
            if (onClickUserInfoItem!=null){
                        onClickUserInfoItem.onClickUserInfoItem(userInfo);
                    }
            */
        } else if (nType == TYPE_ITEM_PHONE_INFO){
            PhoneInfoViewHolder phoneInfoViewHolder = (PhoneInfoViewHolder) holder;
            PhoneInfo userInfo = (PhoneInfo) mArrShow.get(position-1);
            phoneInfoViewHolder.phoneTextView.setText(userInfo.getPhone());
        } else if (nType == TYPE_ITEM_TOP){
            TopViewHolder topViewHolder = (TopViewHolder) holder;
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            topViewHolder.timeTextView.setText(currentDateTimeString);
        } else if (nType == TYPE_ITEM_BOTTOM){
            BottomViewHolder bottomViewHolder = (BottomViewHolder) holder;
            bottomViewHolder.countTextView.setText("Count: " + (getItemCount()-2));
        }
    }

    @Override
    public int getItemViewType(int position) {
        int nResult = TYPE_ITEM_USER_INFO;
        if (position==0){
            nResult = TYPE_ITEM_TOP;
        } else if (position==(getItemCount()-1)){
            nResult = TYPE_ITEM_BOTTOM;
        } else if (mArrShow.get(position-1) instanceof PhoneInfo){
            nResult = TYPE_ITEM_PHONE_INFO;
        }
        return nResult;
    }

    public void setData(ArrayList<UserInfo> arrData){
        mArrData = arrData;
        mArrShow.clear();
        mArrShow.addAll(mArrData);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mArrShow.size() + 2;
    }

    public void setOnClickUserInfoItemListener(OnClickUserInfoItem onClickUserInfoItem) {
        this.onClickUserInfoItem = onClickUserInfoItem;
    }

    private class TopViewHolder extends RecyclerView.ViewHolder{

        TextView timeTextView;

        public TopViewHolder(View itemView) {
            super(itemView);
            timeTextView = (TextView) itemView.findViewById(R.id.timeTextViewTopItem);

        }
    }

    private class BottomViewHolder extends RecyclerView.ViewHolder{

        TextView countTextView;

        public BottomViewHolder(View itemView) {
            super(itemView);
            countTextView = (TextView) itemView.findViewById(R.id.countTextViewBottomItem);

        }
    }

    private class UserInfoViewHolder extends RecyclerView.ViewHolder{

        TextView nameTextView;

        public UserInfoViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.textViewFullNameItemUserInfo);
        }
    }

    private class PhoneInfoViewHolder extends RecyclerView.ViewHolder{

        TextView phoneTextView;

        public PhoneInfoViewHolder(View itemView) {
            super(itemView);
            phoneTextView = (TextView) itemView.findViewById(R.id.textViewPhoneItemPhoneInfo);

        }
    }

    public interface OnClickUserInfoItem{
        void onClickUserInfoItem(UserInfo userInfo);
    }
}
