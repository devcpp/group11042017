package com.kkrasylnykov.l14_savedataexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfo extends BaseEntity {
    private String mName;
    private String mSName;
    private ArrayList<PhoneInfo> mArrPhones;
    private String mAddress;
    private boolean mIsExpand = false;

    public UserInfo(String mName, String mSName, ArrayList<PhoneInfo> mArrPhones, String mAddress) {
        this(-1, mName, mSName, mArrPhones, mAddress);
    }

    public UserInfo(long mId, String mName, String mSName, ArrayList<PhoneInfo> mArrPhones, String mAddress) {
        super();
        setId(mId);
        this.mName = mName;
        this.mSName = mSName;
        this.mArrPhones = mArrPhones;
        this.mAddress = mAddress;
    }

    public UserInfo(Cursor cursor) {
        super(cursor);
        this.mName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.NAME));
        this.mSName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.SNAME));
        this.mArrPhones = new ArrayList<>();
        this.mAddress = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS));
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return mArrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> mArrPhones) {
        this.mArrPhones = mArrPhones;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public boolean isExpand() {
        return mIsExpand;
    }

    public void changeExpand() {
        this.mIsExpand = !mIsExpand;
    }

    @Override
    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.NAME, getName());
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.SNAME, getSName());
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS, getAddress());
        return values;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null){
            if (obj instanceof UserInfo){
                UserInfo userInfo = (UserInfo) obj;
                bResult = this.getId() == userInfo.getId();
            } else if (obj instanceof Long){
                long nId = (long) obj;
                bResult = this.getId() == nId;
            } else if (obj instanceof String){
                long nId = Long.parseLong((String) obj);
                bResult = this.getId() == nId;
            }
        }
        return bResult;
    }
}
