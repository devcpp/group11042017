package com.kkrasylnykov.l14_savedataexample.model.engines;

import android.content.Context;

public abstract class BaseEngine {

    Context mContext;

    public BaseEngine(Context mContext){
        this.mContext = mContext;
    }

    public Context getContext() {
        return mContext;
    }
}
