package com.kkrasylnykov.l12_fragmentorientationexample.activities;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l12_fragmentorientationexample.R;
import com.kkrasylnykov.l12_fragmentorientationexample.fragments.ContentFragment;
import com.kkrasylnykov.l12_fragmentorientationexample.fragments.ListFragment;

public class MainActivity extends AppCompatActivity {

    private ListFragment listFragment;
    private ContentFragment contentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment = new ListFragment();
        contentFragment = new ContentFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.listFrameLayout, listFragment);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            transaction.add(R.id.contentFrameLayout, contentFragment);
        }
        transaction.commit();
    }

    public void setContent(String strContent){
        contentFragment.setContent(strContent);
        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.listFrameLayout, contentFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }
}
