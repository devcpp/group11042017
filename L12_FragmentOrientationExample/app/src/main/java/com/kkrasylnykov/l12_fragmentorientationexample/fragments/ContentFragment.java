package com.kkrasylnykov.l12_fragmentorientationexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentorientationexample.R;



public class ContentFragment extends Fragment {
    private TextView textView = null;
    private String steText = null;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        textView = (TextView) view.findViewById(R.id.textViewContent);
        if (steText!=null){
            textView.setText(steText);
        }
        return view;
    }

    public void setContent(String strContent){
        steText = strContent;
        if (textView!=null){
            textView.setText(steText);
        }
    }
}
