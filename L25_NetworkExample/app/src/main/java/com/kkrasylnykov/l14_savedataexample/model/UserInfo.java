package com.kkrasylnykov.l14_savedataexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserInfo extends BaseEntity {
    private long mServerId;
    private String mName;
    private String mSName;
    private ArrayList<PhoneInfo> mArrPhones;
    private String mAddress;
    private boolean mIsExpand = false;

    public UserInfo(String mName, String mSName, ArrayList<PhoneInfo> mArrPhones, String mAddress) {
        this(-1, mName, mSName, mArrPhones, mAddress);
    }

    public UserInfo(long mId, String mName, String mSName, ArrayList<PhoneInfo> mArrPhones, String mAddress) {
        super();
        setId(mId);
        this.mServerId = -1;
        this.mName = mName;
        this.mSName = mSName;
        this.mArrPhones = mArrPhones;
        this.mAddress = mAddress;
    }

    public UserInfo(Cursor cursor) {
        super(cursor);
        this.mServerId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.SERVER_ID));
        this.mName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.NAME));
        this.mSName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.SNAME));
        this.mArrPhones = new ArrayList<>();
        this.mAddress = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS));
    }

    public UserInfo(JSONObject jsonObject) throws JSONException {
        super();
        this.mServerId = jsonObject.getLong("id");
        this.mName = jsonObject.getString("name");
        this.mSName = jsonObject.getString("sname");;
        this.mArrPhones = new ArrayList<>();
        JSONArray jsonPhonesArray = jsonObject.getJSONArray("phones");
        int nPhoneCount = jsonPhonesArray.length();
        for(int i=0;i<nPhoneCount;i++){
            this.mArrPhones.add(new PhoneInfo(jsonPhonesArray.getString(i)));
        }
        this.mAddress = "";
        JSONArray jsonAddressArray = jsonObject.getJSONArray("address");
        if (jsonAddressArray.length()>0){
            this.mAddress = jsonAddressArray.getString(0);
        }
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return mArrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> mArrPhones) {
        this.mArrPhones = mArrPhones;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public boolean isExpand() {
        return mIsExpand;
    }

    public void changeExpand() {
        this.mIsExpand = !mIsExpand;
    }

    public long getServerId() {
        return mServerId;
    }

    public void setServerId(long mServerId) {
        this.mServerId = mServerId;
    }

    @Override
    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.SERVER_ID, getServerId());
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.NAME, getName());
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.SNAME, getSName());
        values.put(DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS, getAddress());
        return values;
    }

    public String toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (getServerId()!=-1){
            jsonObject.put("id", getServerId());
        }
        jsonObject.put("name", getName());
        jsonObject.put("sname", getSName());
        JSONArray arrAdress = new JSONArray();
        arrAdress.put(getAddress());
        jsonObject.put("address", arrAdress);
        JSONArray arrPhones = new JSONArray();
        for (PhoneInfo item:getPhones()){
            arrPhones.put(item.getPhone());
        }
        jsonObject.put("phones", arrPhones);

        return jsonObject.toString();
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null){
            if (obj instanceof UserInfo){
                UserInfo userInfo = (UserInfo) obj;
                bResult = this.getId() == userInfo.getId();
            } else if (obj instanceof Long){
                long nId = (long) obj;
                bResult = this.getId() == nId;
            } else if (obj instanceof String){
                long nId = Long.parseLong((String) obj);
                bResult = this.getId() == nId;
            }
        }
        return bResult;
    }
}
