package com.kkrasylnykov.l14_savedataexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class DbContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.kkrasylnykov.l14_savedataexample.providers.DbContentProvidersdijvalifhgib";

    public static final String TABLE_USER_INFO  = "user_info";
    public static final String TABLE_PHONES     = "phones";

    public static final Uri USER_INFO_URI       = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER_INFO);
    public static final Uri PHONES_URI          = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONES);

    private static final int URI_USER_INFO          = 1;
    private static final int URI_PHONES             = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, TABLE_USER_INFO, URI_USER_INFO);
        uriMatcher.addURI(AUTHORITY, TABLE_PHONES, URI_PHONES);
    }

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        Log.d("devcpp", "DbContentProvider -> onCreate -> ");
        dbHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String strResult = "";
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                strResult = DBConstants.DB_V2.TableUserInfo.TABLE_NAME;
                break;
            case URI_PHONES:
                strResult = DBConstants.DB_V2.TablePhones.TABLE_NAME;
                break;
        }
        return strResult;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return dbHelper.getReadableDatabase().query(getType(uri), projection, selection, selectionArgs,
                null, null, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long nId = dbHelper.getWritableDatabase().insert(getType(uri), null, values);
        return Uri.parse(uri.toString() + "/" + nId);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return dbHelper.getWritableDatabase().delete(getType(uri), selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        return dbHelper.getWritableDatabase().update(getType(uri), values, selection, selectionArgs);
    }
}
