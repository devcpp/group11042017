package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.engines.UserInfoEngine;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String KEY_ID = "KEY_ID";

    private EditText mNameEditText;
    private EditText mSNameEditText;
    private EditText mPhoneEditText;
    private EditText mAddressEditText;
    private LinearLayout mPhoneContainer;
    private long nId = -1;
    private long nServerId = -1;

    LinearLayout.LayoutParams paramsLinearLayout;
    LinearLayout.LayoutParams paramsEditText;
    LinearLayout.LayoutParams paramsButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mNameEditText = (EditText) findViewById(R.id.nameEditTextEditActivity);
        mSNameEditText = (EditText) findViewById(R.id.snameEditTextEditActivity);
        mAddressEditText = (EditText) findViewById(R.id.addrEditTextEditActivity);
        mPhoneContainer = (LinearLayout) findViewById(R.id.phoneContainer);

        paramsLinearLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsEditText = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,1);
        paramsButton = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 0);

        Button btnAdd = (Button) findViewById(R.id.addButtonEditActivity);
        btnAdd.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                nId = bundle.getLong(KEY_ID, -1);
            }
        }

        if (nId != -1) {
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.removeButtonEditActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = userInfoEngine.getUserById(nId);
            nServerId = userInfo.getServerId();
            mNameEditText.setText(userInfo.getName());
            mSNameEditText.setText(userInfo.getSName());
            mAddressEditText.setText(userInfo.getAddress());

            ArrayList<PhoneInfo> arrPhones = userInfo.getPhones();
            for(PhoneInfo item:arrPhones){
                addPhoneField(item);
            }
        }

        addPhoneField();
    }

    private void addPhoneField(){
        addPhoneField(null);
    }

    private void addPhoneField(PhoneInfo phoneInfo){
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(paramsLinearLayout);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        EditText editText = new EditText(this);
        editText.setLayoutParams(paramsEditText);
        Button button = new Button(this);
        button.setLayoutParams(paramsButton);
        linearLayout.addView(editText);
        linearLayout.addView(button);

        if (phoneInfo!=null){
            editText.setText(phoneInfo.getPhone());
            linearLayout.setTag(phoneInfo.getId());
            button.setText("-");
        } else {
            button.setText("+");
            linearLayout.setTag(new Long(-1));
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strText = ((TextView) v).getText().toString();
                if (strText.equals("-")){
                    linearLayout.setVisibility(View.GONE);
                } else {
                    ((TextView) v).setText("-");
                    addPhoneField();
                }
            }
        });

        mPhoneContainer.addView(linearLayout);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.addButtonEditActivity){
            String strName = mNameEditText.getText().toString().trim();
            String strSName = mSNameEditText.getText().toString().trim();
            String strAddress = mAddressEditText.getText().toString().trim();

            ArrayList<PhoneInfo> arrPhones = new ArrayList<>();
            int nCount = mPhoneContainer.getChildCount();
            for (int i=0; i<nCount; i++){
                LinearLayout child = (LinearLayout) mPhoneContainer.getChildAt(i);
                long nChildTag = (long) child.getTag();

                if (child.getVisibility()==View.VISIBLE){
                    EditText textView = (EditText) child.getChildAt(0);
                    String strPhone = textView.getText().toString().trim();
                    if (!strPhone.isEmpty()){
                        arrPhones.add(new PhoneInfo(nChildTag,nId,strPhone));
                    }
                } else if (nChildTag!=-1) {
                    PhoneInfo phoneInfo = new PhoneInfo(nChildTag,nId,"");
                    phoneInfo.setIsDeleted(true);
                    arrPhones.add(phoneInfo);
                }
            }

            if (strName.isEmpty()){
                Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
                return;
            }

            final UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            final UserInfo userInfo = new UserInfo(nId, strName, strSName, arrPhones, strAddress);
            userInfo.setServerId(nServerId);

            if (userInfo.getId()!=-1){
                Thread threadUpdate = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userInfoEngine.update(userInfo);
                        finish();
                    }
                });
                threadUpdate.start();
            } else {
                Thread threadInsert = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        userInfoEngine.insert(userInfo);
                    }
                });
                threadInsert.start();
            }


            mNameEditText.setText("");
            mNameEditText.requestFocus();
            mSNameEditText.setText("");
            mAddressEditText.setText("");
            mPhoneContainer.removeAllViews();
            addPhoneField();

        } else if (v.getId()==R.id.removeButtonEditActivity) {
            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            userInfoEngine.remove(nId);
            finish();
        }
    }
}
