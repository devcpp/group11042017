package com.kkrasylnykov.l14_savedataexample.model.engines;

import android.content.Context;

import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers.PhonesDBWrapper;
import com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class PhonesEngine extends BaseEngine {
    public PhonesEngine(Context mContext) {
        super(mContext);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nUserId){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        return phonesDBWrapper.getPhonesByUserId(nUserId);
    }

    public void insert(PhoneInfo item){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.insert(item);
    }

    public void update(PhoneInfo item){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.update(item);
    }

    public void remove(long nId){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.remove(nId);
    }

    public void removeByUserId(long nUserId){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.removeByUserId(nUserId);
    }

    public void remove(PhoneInfo item){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.remove(item);
    }

    public void removeAll(){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.removeAll();
    }
}
