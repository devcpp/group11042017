package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.providers.DbContentProvider;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DbContentProvider.USER_INFO_URI);
    }

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        Cursor cursor = getContentResolver().query(getUri(), null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        return arrResult;
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        strSearch = strSearch + "%";
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.NAME + " LIKE ? OR "
                + DBConstants.DB_V2.TableUserInfo.Fields.SNAME + " LIKE ? OR "
                + DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS + " LIKE ?";
        String[] arrArg = new String[]{strSearch, strSearch, strSearch};
        Cursor cursor = getContentResolver().query(getUri(), null, strQuery, arrArg, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo result = null;
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = getContentResolver().query(getUri(), null, strQuery, arrArg, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    result = new UserInfo(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        return result;
    }

    public long insert(UserInfo item){
        Uri uri = getContentResolver().insert(getUri(), item.getContentValues());
        long nUserId = Long.parseLong(uri.getLastPathSegment());
        return nUserId;
    }

    public void update(UserInfo item){
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        getContentResolver().update(getUri(), item.getContentValues(), strQuery, arrArg);
    }

    public void remove(UserInfo item){
        remove(item.getId());
    }

    public void remove(long nId){
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        getContentResolver().delete(getUri(), strQuery, arrArg);
    }

    public void removeAll(){
        getContentResolver().delete(getUri(), null, null);
    }
}
