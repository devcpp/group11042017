package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;

public abstract class BaseDBWrapper {

    private ContentResolver contentResolver;
    private Uri tableUri;

    public BaseDBWrapper(Context context, Uri tableUri){
        contentResolver = context.getContentResolver();
        this.tableUri = tableUri;
    }

    public Uri getUri(){
        return tableUri;
    }

    public ContentResolver getContentResolver(){
        return contentResolver;
    }
}
