package com.kkrasylnykov.l14_savedataexample.model.engines;

import android.content.Context;

import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers.UserInfoDBWrapper;
import com.kkrasylnykov.l14_savedataexample.model.wrappers.networkWrappers.UserInfoNetworkWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {
    public UserInfoEngine(Context mContext) {
        super(mContext);
    }

    public ArrayList<UserInfo> getAllUsers(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = userInfoDBWrapper.getAllUsers();
        if (arrData==null || arrData.size()==0){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper();
            arrData = networkWrapper.getAllUsers();
            if (arrData!=null && arrData.size()>0){
                insert(arrData);
                return getAllUsers();
            } else {
                return arrData;
            }
        } else {
            return getUsersWithPhone(arrData);
        }
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return getUsersWithPhone(userInfoDBWrapper.getUsersBySearch(strSearch));
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        UserInfo userInfo = userInfoDBWrapper.getUserById(nId);
        PhonesEngine phonesEngine = new PhonesEngine(getContext());
        userInfo.setPhones(phonesEngine.getPhonesByUserId(userInfo.getId()));
        return userInfo;
    }

    private ArrayList<UserInfo> getUsersWithPhone(ArrayList<UserInfo> arrData){
        if (arrData!=null && arrData.size()>0){
            PhonesEngine phonesEngine = new PhonesEngine(getContext());
            for (UserInfo userInfo:arrData){
                userInfo.setPhones(phonesEngine.getPhonesByUserId(userInfo.getId()));
            }
        }
        return arrData;
    }

    public void insert(ArrayList<UserInfo> items){
        for (UserInfo item:items){
            insert(item, false);
        }
    }

    public void insert(UserInfo item){
        insert(item, true);
    }

    public void insert(UserInfo item, boolean isSendToServer){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        long nUserId = userInfoDBWrapper.insert(item);
        ArrayList<PhoneInfo> arrPhones = item.getPhones();
        if (arrPhones!=null && arrPhones.size()>0){
            PhonesEngine phonesEngine = new PhonesEngine(getContext());
            for (PhoneInfo phoneInfo:arrPhones){
                phoneInfo.setUserId(nUserId);
                phonesEngine.insert(phoneInfo);
            }
        }

        if (isSendToServer){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper();
            long nServerId = networkWrapper.insert(item);

            item.setServerId(nServerId);
            update(item, false);
        }
    }

    public void update(UserInfo item){
        update(item, true);
    }

    public void update(UserInfo item, boolean isSendToServer){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.update(item);
        ArrayList<PhoneInfo> arrPhones = item.getPhones();
        if (arrPhones!=null && arrPhones.size()>0){
            PhonesEngine phonesEngine = new PhonesEngine(getContext());
            for (PhoneInfo phoneInfo:arrPhones){
                if(phoneInfo.getId()>-1){
                    if (phoneInfo.isDeleted()){
                        phonesEngine.remove(phoneInfo);
                    } else {
                        phonesEngine.update(phoneInfo);
                    }
                } else {
                    phoneInfo.setUserId(item.getId());
                    phonesEngine.insert(phoneInfo);

                }
            }
        }
        if (isSendToServer){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper();
            networkWrapper.update(item);
        }


    }

    public void remove(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.remove(nId);
        PhonesEngine phonesEngine = new PhonesEngine(getContext());
        phonesEngine.removeByUserId(nId);
    }

    public void remove(UserInfo item){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.remove(item);
        PhonesEngine phonesEngine = new PhonesEngine(getContext());
        phonesEngine.removeByUserId(item.getId());
    }

    public void removeAll(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeAll();
        PhonesEngine phonesEngine = new PhonesEngine(getContext());
        phonesEngine.removeAll();
    }
}
