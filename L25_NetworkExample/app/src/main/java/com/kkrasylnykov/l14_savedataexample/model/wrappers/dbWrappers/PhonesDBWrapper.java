package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.providers.DbContentProvider;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhonesDBWrapper extends BaseDBWrapper {

    public PhonesDBWrapper(Context context) {
        super(context, DbContentProvider.PHONES_URI);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nUserId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.USER_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nUserId)};
        Cursor cursor = getContentResolver().query(getUri(), null, strQuery, arrArg, null);
        if (cursor!=null){
            try {
                if (cursor.moveToFirst()){
                    do {
                        arrResult.add(new PhoneInfo(cursor));
                    } while (cursor.moveToNext());
                }

            }finally {
                cursor.close();
            }
        }
        return arrResult;
    }

    public void insert(PhoneInfo item){
        getContentResolver().insert(getUri(), item.getContentValues());
    }

    public void update(PhoneInfo item){
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        getContentResolver().update(getUri(), item.getContentValues(), strQuery, arrArg);
    }

    public void remove(long nId){
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        getContentResolver().delete(getUri(), strQuery, arrArg);
    }

    public void removeByUserId(long nUserId){
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.USER_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nUserId)};
        getContentResolver().delete(getUri(), strQuery, arrArg);
    }

    public void remove(PhoneInfo item){
        remove(item.getId());
    }

    public void removeAll(){
        getContentResolver().delete(getUri(), null, null);
    }
}
