package l02_functionexample;

import java.util.Scanner;

public class L02_FunctionExample {
    
    public static int meanSquare(int v){
        if(v<0){
            return 0;
        }
        int nResult = ((v * v * v * v)/4)*8;
        return nResult;
    }
    
    public static float meanSquare(float v){
        float fResult = ((v * v * v * v)/4)*8;
        return fResult;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int v1 = scanner.nextInt();
        int nRusult1 = meanSquare(v1);
        System.out.println("nRusult1 = " + nRusult1);
        
        /*
        .....
        */
        
        int v2 = scanner.nextInt();
        int nRusult2 = meanSquare(v2);
        System.out.println("nRusult2 = " + nRusult2);
        
        /*
        .....
        */
        
        int v3 = v2 + nRusult1;
        int nRusult3 = meanSquare(v3);
        System.out.println("nRusult3 = " + nRusult3);   
        
        float v4 = scanner.nextFloat();
        float nRusult4 = meanSquare(v4);
        System.out.println("nRusult4 = " + nRusult4);   
    }
}
