package com.logachev.yegor.animationsample;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by yegor on 2/22/17.
 */

public class ScreenTransitionsFragment1 extends Fragment {

    public static Fragment newInstance() {
        return new ScreenTransitionsFragment1();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_transition_1, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(new GridAdapter(getContext()));
        return view;
    }

    class GridAdapter extends RecyclerView.Adapter<GridViewHolder> {

        LayoutInflater inflater;

        GridAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.item_view_pager, parent, false);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = parent.getMeasuredHeight() / 3;
            view.setLayoutParams(layoutParams);
            return new GridViewHolder(view);
        }

        @Override
        public void onBindViewHolder(GridViewHolder holder, int position) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imageView.setTransitionName("imageTransitionName" + position);
            }
            holder.imageView.setImageResource(MainActivity.SRC_IMGS_RESOURCES[position]);
        }

        @Override
        public int getItemCount() {
            return MainActivity.SRC_IMGS_RESOURCES.length;
        }
    }
    class GridViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;

        GridViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView;
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setEnterTransition(new Explode());
                setExitTransition(new Explode());
                fragmentTransaction.addSharedElement(v, v.getTransitionName());
                fragmentTransaction.replace(R.id.container, ScreenTransitionsFragment2.newInstance(getAdapterPosition(), v.getTransitionName()));

            } else {
                fragmentTransaction.replace(R.id.container, ScreenTransitionsFragment2.newInstance(getAdapterPosition(), ""));
            }
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();



        }
    }
}
