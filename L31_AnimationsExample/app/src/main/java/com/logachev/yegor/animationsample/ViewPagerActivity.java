package com.logachev.yegor.animationsample;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by yegor on 2/16/17.
 */

public class ViewPagerActivity extends AppCompatActivity {

    private static final float MAX_SCALE = 0.2f;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setPageMargin((int) (16 * getResources().getDisplayMetrics().density + 0.5f));
        viewPager.setAdapter(new ViewPagerAdapter(this));
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                float scaleFactor = (1 - MAX_SCALE) * (1 - Math.abs(position)) + MAX_SCALE;
                page.setScaleX(scaleFactor);
                page.setScaleY(scaleFactor);
            }
        });
    }

    class ViewPagerAdapter extends PagerAdapter {

        private LayoutInflater inflater;

        ViewPagerAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return MainActivity.SRC_IMGS_RESOURCES.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView item = (ImageView) inflater.inflate(R.layout.item_view_pager, container, false);
            item.setImageResource(MainActivity.SRC_IMGS_RESOURCES[position]);
            container.addView(item);
            return item;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
