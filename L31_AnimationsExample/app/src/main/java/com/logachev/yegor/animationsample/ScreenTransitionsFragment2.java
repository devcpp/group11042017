package com.logachev.yegor.animationsample;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.ChangeBounds;
import android.transition.ChangeClipBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by yegor on 2/22/17.
 */

public class ScreenTransitionsFragment2 extends Fragment implements View.OnClickListener {

    private static final String POSITION_BUNDLE_KEY = "position_bundle_key";
    private static final String TRANSITION_NAME_BUNDLE_KEY = "transition_name_bundle_key";

    private ImageView imageView;

    public static Fragment newInstance(int position, String transitionName) {
        ScreenTransitionsFragment2 screenTransitionsFragment2 = new ScreenTransitionsFragment2();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION_BUNDLE_KEY, position);
        bundle.putString(TRANSITION_NAME_BUNDLE_KEY, transitionName);
        screenTransitionsFragment2.setArguments(bundle);

        TransitionSet set = new TransitionSet();
        set.addTransition(new ChangeBounds());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            set.addTransition(new ChangeTransform());
            set.addTransition(new ChangeClipBounds());
            set.addTransition(new ChangeImageTransform());
        }

        screenTransitionsFragment2.setSharedElementEnterTransition(set);
        screenTransitionsFragment2.setSharedElementReturnTransition(set);
        return screenTransitionsFragment2;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_transition_2, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        Bundle arguments = getArguments();
        int position = arguments.getInt(POSITION_BUNDLE_KEY);
        String transitionName = arguments.getString(TRANSITION_NAME_BUNDLE_KEY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setTransitionName(transitionName);
        }
        imageView.setImageResource(MainActivity.SRC_IMGS_RESOURCES[position]);
        view.findViewById(R.id.buttonClickMe).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        setImageVisible(!imageView.isShown());
    }

    private Animator createHideAnimator(final View view) {
        int width = view.getWidth();
        int height = view.getHeight();
        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            circularReveal = ViewAnimationUtils.createCircularReveal(view, width / 2, view.getBottom(), (float) Math.hypot(width, height), 0f);
        }
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });
        return circularReveal;
    }

    private Animator createShowAnimator(final View view) {
        int width = view.getWidth();
        int height = view.getHeight();
        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            circularReveal = ViewAnimationUtils.createCircularReveal(view, width / 2, view.getBottom(), 0f, (float) Math.hypot(width, height));
        }
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(View.VISIBLE);
            }
        });
        return circularReveal;
    }

    private void setImageVisible(boolean visibility) {
        Animator animator = visibility ? createShowAnimator(imageView) : createHideAnimator(imageView);
        animator.start();
    }
}
