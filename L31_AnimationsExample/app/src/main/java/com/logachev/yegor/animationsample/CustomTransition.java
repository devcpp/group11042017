package com.logachev.yegor.animationsample;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by yegor on 2/15/17.
 */

public class CustomTransition extends Transition {

    private static final String START_POSITION_X = "com.logachev.yegor.animationsample.CustomTransition.START_POSITION_X";
    private static final String START_POSITION_Y = "com.logachev.yegor.animationsample.CustomTransition.START_POSITION_Y";
    private static final String END_POSITION_Y = "com.logachev.yegor.animationsample.CustomTransition.END_POSITION_Y";
    private static final String END_POSITION_X = "com.logachev.yegor.animationsample.CustomTransition.END_POSITION_X";


    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        transitionValues.values.put(START_POSITION_X, view.getX());
//        transitionValues.values.put(START_POSITION_Y, view.getY());
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        transitionValues.values.put(END_POSITION_X, 200f);
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
        return ObjectAnimator.ofFloat(startValues.view, "x", (float) startValues.values.get(START_POSITION_X), (float) endValues.values.get(END_POSITION_X));
    }
}
