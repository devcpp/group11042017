package com.logachev.yegor.animationsample;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Created by yegor on 2/15/17.
 */

public class TransitionAnimActivity extends AppCompatActivity implements View.OnClickListener {

    FrameLayout rootView;
    int clickCounter = 0;
    Scene scene1;
    Scene scene2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition_anim);
        rootView = (FrameLayout) findViewById(R.id.rootView);
        rootView.setOnClickListener(this);
        scene1 = Scene.getSceneForLayout(rootView, R.layout.sample_scene_1, this);
        scene2 = Scene.getSceneForLayout(rootView, R.layout.sample_scene_2, this);


    }

    @Override
    public void onClick(View v) {
        clickCounter %= 3;
        switch (clickCounter++) {
            case 0:
                TransitionManager.go(scene2, new ChangeBounds());
                break;

            case 1:
                TransitionManager.go(scene1, new ChangeBounds());
                break;

            case 2:
                TransitionManager.beginDelayedTransition(rootView);
                changeViewSize(R.id.greenView, 200, 400);
                changeViewSize(R.id.redView, 400, 200);
                changeViewSize(R.id.blueView, 200, 400);
                changeViewSize(R.id.grayView, 400, 200);
                break;
        }
        TransitionManager.go(new Scene(rootView), new CustomTransition());
    }

    private void changeViewSize(@IdRes int viewRes, int width, int height) {
        View viewById = findViewById(viewRes);
        ViewGroup.LayoutParams layoutParams = viewById.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        viewById.setLayoutParams(layoutParams);
    }
}
