package com.logachev.yegor.animationsample;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by yegor on 2/16/17.
 */

public class ViewAnimActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_animation);
        CarouselRecyclerView recyclerView = (CarouselRecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CarouselAdapter(this));
    }

    /*Adapter Class*/
    static class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.CarouselViewHolder> {

        private LayoutInflater inflater;
        private List<String> countries;

        public CarouselAdapter(Context context) {
            Locale[] locales = Locale.getAvailableLocales();
            countries = new ArrayList<>();
            for (Locale locale : locales) {
                String country = locale.getDisplayCountry();
                if (country.trim().length()>0 && !countries.contains(country)) {
                    countries.add(country);
                }
            }
            Collections.sort(countries);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public CarouselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View item = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            return new CarouselViewHolder(item);
        }

        @Override
        public void onBindViewHolder(CarouselViewHolder holder, int position) {
            holder.textView.setText(countries.get(position));
        }

        @Override
        public int getItemCount() {
            return countries.size();
        }

        /*Holder Class*/
        class CarouselViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            CarouselViewHolder(View itemView) {
                super(itemView);
                textView = (TextView) itemView;
                textView.setBackgroundColor(Color.GREEN);
                textView.setTextSize(30f);
                textView.setGravity(Gravity.CENTER);
            }
        }
    }
}
