package com.logachev.yegor.animationsample;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;

/**
 * Created by Yegor on 2/13/17.
 */

public class PropertyAnimActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_anim);
        findViewById(R.id.btnValueAnim).setOnClickListener(this);
        findViewById(R.id.btnObjectAnim).setOnClickListener(this);
        findViewById(R.id.btnViewAnim).setOnClickListener(this);
        findViewById(R.id.btnComplexAnim).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnValueAnim:
                onValueAnimClick(view);
                break;

            case R.id.btnObjectAnim:
                onObjectAnimClick(view);
                break;

            case R.id.btnViewAnim:
                onViewAnimClick(view);
                break;

            case R.id.btnComplexAnim:
                onComplexAnimClick(view);
                break;

        }
    }

    private void onValueAnimClick(View view) {
        final Button btn = (Button) view;
        final float defTextSize = btn.getTextSize();
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(10f, 25f).setDuration(2000);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                btn.setTextSize(value);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, defTextSize);
            }
        });
        valueAnimator.start();
    }

    private void onObjectAnimClick(View view) {
        Keyframe kf0 = Keyframe.ofFloat(0f, 0f);
        Keyframe kf1 = Keyframe.ofFloat(0.3f, 180f);
        Keyframe kf2 = Keyframe.ofFloat(0.6f, 90f);
        Keyframe kf3 = Keyframe.ofFloat(1f, 360f);
        PropertyValuesHolder holder = PropertyValuesHolder.ofKeyframe("rotation", kf0, kf1, kf2, kf3);
        ObjectAnimator.ofPropertyValuesHolder(view, holder).setDuration(10000).start();
    }

    private void onViewAnimClick(final View view) {
        view.animate().translationY(300).setDuration(2000).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                view.setTranslationY(0);
            }
        });
    }

    private void onComplexAnimClick(View view) {
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.compex_animator);
        set.setTarget(view);
        set.start();
    }
}
