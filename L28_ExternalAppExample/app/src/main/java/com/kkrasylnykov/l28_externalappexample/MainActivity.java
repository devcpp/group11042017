package com.kkrasylnykov.l28_externalappexample;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPhoneOnExternalApp();

        editText = (EditText) findViewById(R.id.dataEditText);
        textView = (TextView) findViewById(R.id.dataTextView);

        ((EditText)findViewById(R.id.emailSearch)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strSearch = s.toString();
                if (textView!=null){
                    textView.setText(getData(strSearch));
                }
            }
        });

        findViewById(R.id.btnWeb).setOnClickListener(this);
        findViewById(R.id.btnGeo).setOnClickListener(this);
        findViewById(R.id.btnTel).setOnClickListener(this);
        findViewById(R.id.btnMail).setOnClickListener(this);
    }

    private String getData(String strSearch){
        String strResult = "";

        String strSearchEmail = "%" + strSearch + "%";
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.Contacts.HAS_PHONE_NUMBER};

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                PROJECTION,
                ContactsContract.CommonDataKinds.Email.DATA + " LIKE ?",
                new String[]{strSearchEmail}, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do {
                    strResult += "\n";
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                    String strName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String strEmail = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    strResult += id + "\t" + strName + "\t\t" + strEmail;
                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0){
                        strResult +="\n";

                        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        if (pCur!=null){
                            if (pCur.moveToFirst()){
                                do {
                                    String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    strResult += contactNumber + "\t";
                                } while (pCur.moveToNext());
                            }
                            pCur.close();
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return strResult;
    }

    @Override
    public void onClick(View v) {
        String strData = editText.getText().toString();
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.btnWeb:
                intent.setAction(Intent.ACTION_VIEW);
                if (strData.indexOf("http")!=0){
                    strData = "http://" + strData;
                }
                intent.setData(Uri.parse(strData));
                break;
            case R.id.btnGeo:
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:46.993,37.239"));
                break;
            case R.id.btnTel:
                //intent.setAction(Intent.ACTION_DIAL);
                intent.setAction(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + strData));
                break;
            case R.id.btnMail:
//                Uri uri = Uri.parse("smsto:0954065738");
//                intent = new Intent(Intent.ACTION_SENDTO, uri);
//                intent.putExtra("sms_body", "The SMS text");
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"kos@example.com"});
                intent.putExtra(Intent.EXTRA_CC  , new String[]{"kos123@example.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                intent.putExtra(Intent.EXTRA_TEXT   , strData);
                break;
        }
        startActivity(intent);
    }

    private static final String AUTHORITY = "com.kkrasylnykov.l14_savedataexample.providers.DbContentProvidersdijvalifhgib";

    public static final String TABLE_USER_INFO  = "user_info";
    public static final String TABLE_PHONES     = "phones";

    public static final Uri USER_INFO_URI       = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER_INFO);
    public static final Uri PHONES_URI          = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONES);

    private void getPhoneOnExternalApp(){
        Cursor cursor = getContentResolver().query(USER_INFO_URI, null, null, null, null);

        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    Log.d("devcpp", cursor.getLong(0) + " " + cursor.getString(2) + " " + cursor.getString(3));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        ContentValues values = new ContentValues();
        values.put("_server_id", -1);
        values.put("_name", "Test");
        values.put("_sname", "Content-Provider");
        values.put("_address", "Devichya, 6-1");

        getContentResolver().insert(USER_INFO_URI, values);
    }
}
