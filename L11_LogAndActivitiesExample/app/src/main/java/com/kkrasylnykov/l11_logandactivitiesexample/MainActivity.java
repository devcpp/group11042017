package com.kkrasylnykov.l11_logandactivitiesexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUST_ID_SECOND_ACTIVITY = 101;


    EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("devcpp","onCreate");

        Button button = (Button) findViewById(R.id.btnNewActivityMainActivity);
        button.setOnClickListener(this);

        mEditText = (EditText) findViewById(R.id.nameEditTextMainActivity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","onDestroy");
    }


    @Override
    public void onClick(View v) {
        Log.d("devcpp","onClick");

        switch (v.getId()){
            case R.id.btnNewActivityMainActivity:
                String strData = mEditText.getText().toString();
                Log.d("devcpp","strData -> " + strData);
                Intent intent = new Intent(this, SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_STRING_NAME, strData);
                startActivityForResult(intent, REQUST_ID_SECOND_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("devcpp", "requestCode -> " + requestCode);
        Log.d("devcpp", "resultCode -> " + resultCode);
        Log.d("devcpp", "data -> " + data);

        if (requestCode==REQUST_ID_SECOND_ACTIVITY){
            if (resultCode==RESULT_OK){
                String strTest = "";
                if (data!=null){
                    Bundle bundle = data.getExtras();
                    if (bundle!=null){
                        strTest = bundle.getString(SecondActivity.KEY_STRING_NAME, "");
                    }
                }
                Toast.makeText(this, "Нажали Yes -> " + strTest, Toast.LENGTH_SHORT).show();
            } else if (resultCode==RESULT_CANCELED){
                Toast.makeText(this, "Нажали No", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
