package com.kkrasylnykov.l11_logandactivitiesexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    public  static final String KEY_STRING_NAME = "KEY_STRING_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.d("devcpp","SecondActivity -> onCreate");

        Intent intent = getIntent();

        String strName = "";

        Bundle bundle = intent.getExtras();
        if (bundle!=null){
            strName = bundle.getString(KEY_STRING_NAME, "");
        }

        TextView textView = (TextView) findViewById(R.id.nameTextViewSecondActivity);
        textView.setText("Привет, " + strName + "!");

        findViewById(R.id.positiveButtonSecondActivity).setOnClickListener(this);
        findViewById(R.id.negativeButtonSecondActivity).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","SecondActivity -> onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","SecondActivity -> onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","SecondActivity -> onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","SecondActivity -> onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","SecondActivity -> onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","SecondActivity -> onDestroy");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.positiveButtonSecondActivity:
                Intent intent = new Intent();
                intent.putExtra(KEY_STRING_NAME , "Yes");//По правильному, ключь долен быть разный
                setResult(RESULT_OK, intent);
                break;
            case R.id.negativeButtonSecondActivity:
                setResult(RESULT_CANCELED);
                break;
        }

        finish();
    }
}
