package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;

public class DBConstants {
    public static final String DB_NAME = "db_notepad";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "_UserInfo";

    public static final String FIELD_ID = "_id";
    public static final String FIELD_NAME = "_name";
    public static final String FIELD_SNAME = "_sname";
    public static final String FIELD_PHONE = "_phone";
    public static final String FIELD_ADDRESS = "_address";

}
