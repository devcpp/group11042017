package com.kkrasylnykov.l14_savedataexample.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME + " (" +
                DBConstants.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.FIELD_NAME + " TEXT NOT NULL, " +
                DBConstants.FIELD_SNAME + " TEXT, " +
                DBConstants.FIELD_PHONE + " TEXT, " +
                DBConstants.FIELD_ADDRESS + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
