package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_TC_ACTIVITY = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        DBHelper dbHelper = new DBHelper(this);

        if (appSettings.isShowTC()){
            Intent intent = new Intent(this, TCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC_ACTIVITY);

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values;
            for (int i=0;i<10; i++){
                values = new ContentValues();
                values.put(DBConstants.FIELD_NAME, "Kos" + i);
                values.put(DBConstants.FIELD_SNAME, "Kras" + i);
                values.put(DBConstants.FIELD_PHONE, "095" + i);
                values.put(DBConstants.FIELD_ADDRESS, "Devichia, " + i);
                db.insert(DBConstants.TABLE_NAME, null, values);
            }
            db.close();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_TC_ACTIVITY){
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isShowTC()){
                finish();
            }
        }

    }
}
