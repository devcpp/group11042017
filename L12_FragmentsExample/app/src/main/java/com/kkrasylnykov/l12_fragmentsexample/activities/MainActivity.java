package com.kkrasylnykov.l12_fragmentsexample.activities;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kkrasylnykov.l12_fragmentsexample.R;
import com.kkrasylnykov.l12_fragmentsexample.fragments.FirstFragment;
import com.kkrasylnykov.l12_fragmentsexample.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String FIRST_FRAGMENT = "FIRST_FRAGMENT";
    private final static String SECOND_FRAGMENT = "SECOND_FRAGMENT";

    private FirstFragment firstFragment;
    private SecondFragment secondFragment;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.btnAddFirstFragment);
        button.setOnClickListener(this);
        findViewById(R.id.btnAddSecondFragment).setOnClickListener(this);
        findViewById(R.id.btnRemoveFirstFragment).setOnClickListener(this);
        findViewById(R.id.btnRemoveSecondFragment).setOnClickListener(this);

        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();


    }

    @Override
    public void onClick(View v) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.btnAddFirstFragment:
                if (getFragmentManager().findFragmentByTag(FIRST_FRAGMENT)==null){
                    fragmentTransaction.add(R.id.firstFrameLayout, firstFragment, FIRST_FRAGMENT);
                }

//                fragmentTransaction.replace(R.id.firstFrameLayout, firstFragment, FIRST_FRAGMENT);

                break;
//            case R.id.btnAddFirstFragmentToSecondLayout:
//                if (getFragmentManager().findFragmentByTag(FIRST_FRAGMENT)==null){
//                    fragmentTransaction.add(R.id.secondFrameLayout, firstFragment, FIRST_FRAGMENT);
//                }
////                fragmentTransaction.replace(R.id.secondFrameLayout, firstFragment, FIRST_FRAGMENT);
//                break;
            case R.id.btnAddSecondFragment:
                if (getFragmentManager().findFragmentByTag(SECOND_FRAGMENT)==null){
                    fragmentTransaction.add(R.id.secondFrameLayout, secondFragment, SECOND_FRAGMENT);
                }
                break;
            case R.id.btnRemoveFirstFragment:
                fragmentTransaction.remove(firstFragment);
                button.setVisibility(View.VISIBLE);
                break;
            case R.id.btnRemoveSecondFragment:
                fragmentTransaction.remove(secondFragment);
                break;
        }
        fragmentTransaction.commit();
    }

    public void setInvisible(){
        if (button.getVisibility()==View.INVISIBLE){
            button.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.INVISIBLE);
        }
    }

    public void sendText(String strText){
        secondFragment.setText(strText);
    }

}
