package com.kkrasylnykov.l12_fragmentsexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentsexample.R;

public class SecondFragment extends Fragment {
    TextView textView = null;
    String strText = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        textView = (TextView) view.findViewById(R.id.textViewSecondFragment);
        if (strText!=null){
            textView.setText(strText);
        }
        return view;
    }

    public void setText(String strText){
        this.strText = strText;
        if (textView!=null){
            textView.setText(this.strText);
        }
    }

}
