package com.kkrasylnykov.l12_fragmentsexample.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kkrasylnykov.l12_fragmentsexample.R;
import com.kkrasylnykov.l12_fragmentsexample.activities.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {
    private EditText editText;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        view.findViewById(R.id.btnInvisibleFragmentFirst).setOnClickListener(this);
        view.findViewById(R.id.btnSendTextFragmentFirst).setOnClickListener(this);
        editText = (EditText) view.findViewById(R.id.editTextFragmentFirst);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnInvisibleFragmentFirst:
                Activity activity = getActivity();
                if (activity!=null && activity instanceof MainActivity){
                    ((MainActivity)activity).setInvisible();
                }
                break;
            case R.id.btnSendTextFragmentFirst:
                Activity activitySend = getActivity();
                if (activitySend!=null && activitySend instanceof MainActivity){
                    ((MainActivity)activitySend).sendText(editText.getText().toString());
                }
                break;
        }

    }
}
