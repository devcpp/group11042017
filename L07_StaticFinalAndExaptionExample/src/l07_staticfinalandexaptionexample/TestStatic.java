package l07_staticfinalandexaptionexample;

public class TestStatic {
    
    public static final int MAX_SIZE = 34583;
    
    private static int sCount = 0;
    private String mName = "";
    
    static {
    }
    
    public TestStatic(){ 
        sCount++;
    }
    
    public String getName(){
        if (mName.length()>MAX_SIZE){
           mName = mName.substring(0,MAX_SIZE);
        }
        return mName;
    }
    
    public static int getCount(){
        return sCount;
    }    
}
