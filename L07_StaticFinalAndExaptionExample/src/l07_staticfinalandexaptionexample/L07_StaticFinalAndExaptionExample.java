package l07_staticfinalandexaptionexample;

import java.util.Scanner;

public class L07_StaticFinalAndExaptionExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TestStatic test1 = new TestStatic();
        int n = TestStatic.MAX_SIZE;
        
        double d = Math.PI;
        
        System.out.println("test1 -> " + TestStatic.getCount());
        
        TestStatic test2 = new TestStatic();
        TestStatic test3 = new TestStatic();
        
        System.out.println("test1 -> " + TestStatic.getCount());       
        
        boolean bIsRepeat = false;
        do{
            try{
                int arr[] = {-1, 0 ,1};
                String strData = scanner.nextLine();
                int nPos = Integer.parseInt(strData);
                int nResult = 5/arr[nPos];
                System.out.println("nResult = " + nResult); 
            } catch (NumberFormatException e){
                System.out.println("Вы ввели не число!");
                bIsRepeat = true;
                return;
            } catch (ArrayIndexOutOfBoundsException e){
                System.out.println("Число не должно быть меньше 0 и больше 2");
                bIsRepeat = true;
            } catch (ArithmeticException e){
                System.out.println(e.getMessage());
                bIsRepeat = true;
            } catch (Exception e){
                
            } finally {
                System.out.println("finally");
            }
            
        } while (bIsRepeat);
        
        
        
    
    }
    
}
