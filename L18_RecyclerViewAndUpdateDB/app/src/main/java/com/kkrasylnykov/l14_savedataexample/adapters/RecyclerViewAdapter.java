package com.kkrasylnykov.l14_savedataexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM_USER_INFO = 0;
    private static final int TYPE_ITEM_TOP = 1;
    private static final int TYPE_ITEM_BOTTOM = 2;

    private ArrayList<UserInfo> mArrData;

    private OnClickUserInfoItem onClickUserInfoItem = null;

    public RecyclerViewAdapter(ArrayList<UserInfo> arrData){
        mArrData = arrData;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        if (viewType == TYPE_ITEM_USER_INFO){
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_USER_INFO");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
            holder = new UserInfoViewHolder(view);
        } else if (viewType == TYPE_ITEM_TOP) {
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_TOP");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top, parent, false);
            holder = new TopViewHolder(view);
        } else if (viewType == TYPE_ITEM_BOTTOM) {
            Log.d("devcpp","onCreateViewHolder->TYPE_ITEM_BOTTOM");
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom, parent, false);
            holder = new BottomViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int nType = getItemViewType(position);
        if (nType == TYPE_ITEM_USER_INFO){
            UserInfoViewHolder userInfoViewHolder = (UserInfoViewHolder) holder;
            final UserInfo userInfo = mArrData.get(position-1);
            userInfoViewHolder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
            userInfoViewHolder.phoneTextView.setText(userInfo.getPhone());
            userInfoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickUserInfoItem!=null){
                        onClickUserInfoItem.onClickUserInfoItem(userInfo);
                    }
                }
            });
        } else if (nType == TYPE_ITEM_TOP){
            TopViewHolder topViewHolder = (TopViewHolder) holder;
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            topViewHolder.timeTextView.setText(currentDateTimeString);
        } else if (nType == TYPE_ITEM_BOTTOM){
            BottomViewHolder bottomViewHolder = (BottomViewHolder) holder;
            bottomViewHolder.countTextView.setText("Count: " + (getItemCount()-2));
        }
    }

    @Override
    public int getItemViewType(int position) {
        int nResult = TYPE_ITEM_USER_INFO;
        if (position==0){
            nResult = TYPE_ITEM_TOP;
        } else if (position==(getItemCount()-1)){
            nResult = TYPE_ITEM_BOTTOM;
        }
        return nResult;
    }

    @Override
    public int getItemCount() {
        return mArrData.size() + 2;
    }

    public void setOnClickUserInfoItemListener(OnClickUserInfoItem onClickUserInfoItem) {
        this.onClickUserInfoItem = onClickUserInfoItem;
    }

    private class TopViewHolder extends RecyclerView.ViewHolder{

        TextView timeTextView;

        public TopViewHolder(View itemView) {
            super(itemView);
            timeTextView = (TextView) itemView.findViewById(R.id.timeTextViewTopItem);

        }
    }

    private class BottomViewHolder extends RecyclerView.ViewHolder{

        TextView countTextView;

        public BottomViewHolder(View itemView) {
            super(itemView);
            countTextView = (TextView) itemView.findViewById(R.id.countTextViewBottomItem);

        }
    }

    private class UserInfoViewHolder extends RecyclerView.ViewHolder{

        TextView nameTextView;
        TextView phoneTextView;

        public UserInfoViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.textViewFullNameItemUserInfo);
            phoneTextView = (TextView) itemView.findViewById(R.id.textViewPhoneItemUserInfo);

        }
    }

    public interface OnClickUserInfoItem{
        void onClickUserInfoItem(UserInfo userInfo);
    }
}
