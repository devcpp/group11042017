package com.kkrasylnykov.l14_savedataexample.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;

import java.util.ArrayList;

public class UserInfoAdapter extends BaseAdapter {

    private ArrayList<UserInfo> mArrData;

    public UserInfoAdapter(ArrayList<UserInfo> arrData){
        mArrData = arrData;
    }

    @Override
    public int getCount() {
        return mArrData.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mArrData.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
        }

        UserInfo userInfo = (UserInfo) getItem(position);

        TextView nameTextView = (TextView) convertView.findViewById(R.id.textViewFullNameItemUserInfo);
        TextView phoneTextView = (TextView) convertView.findViewById(R.id.textViewPhoneItemUserInfo);
        nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
        phoneTextView.setText(userInfo.getPhone());
        return convertView;
    }
}
