package com.kkrasylnykov.l14_savedataexample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
        Log.d("devcpp", "DBHelper -> DBConstants.DB_NAME -> " + DBConstants.DB_NAME);
        Log.d("devcpp", "DBHelper -> DBConstants.DB_VERSION -> " + DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("devcpp", "onCreate -> ");
        db.execSQL("CREATE TABLE " + DBConstants.DB_V2.TableUserInfo.TABLE_NAME + " (" +
                DBConstants.DB_V2.TableUserInfo.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.DB_V2.TableUserInfo.Fields.NAME + " TEXT NOT NULL, " +
                DBConstants.DB_V2.TableUserInfo.Fields.SNAME + " TEXT, " +
                DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS + " TEXT);");

        db.execSQL("CREATE TABLE " + DBConstants.DB_V2.TablePhones.TABLE_NAME + " (" +
                DBConstants.DB_V2.TablePhones.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.DB_V2.TablePhones.Fields.USER_ID + " INTEGER, " +
                DBConstants.DB_V2.TablePhones.Fields.PHONE + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("devcpp", "onUpgrade -> oldVersion ->" + oldVersion);
        Log.d("devcpp", "onUpgrade -> newVersion -> " + newVersion);
        if (oldVersion == 1){
            onCreate(db);
            Cursor cursor = db.query(DBConstants.DB_V1.TABLE_NAME, null,null,null,null,null,null);
            if (cursor!=null){
                try {
                    if (cursor.moveToFirst()){
                        do {
                            String strName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V1.FIELD_NAME));
                            String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V1.FIELD_SNAME));
                            String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V1.FIELD_PHONE));
                            String strAddress = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V1.FIELD_ADDRESS));

                            Log.d("devcpp", "onUpgrade -> strName -> " + strName);

                            ContentValues values = new ContentValues();
                            values.put(DBConstants.DB_V2.TableUserInfo.Fields.NAME, strName);
                            values.put(DBConstants.DB_V2.TableUserInfo.Fields.SNAME, strSName);
                            values.put(DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS, strAddress);
                            long nUserId = db.insert(DBConstants.DB_V2.TableUserInfo.TABLE_NAME, null, values);

                            Log.d("devcpp", "onUpgrade -> nUserId -> " + nUserId);

                            values = new ContentValues();
                            values.put(DBConstants.DB_V2.TablePhones.Fields.USER_ID, nUserId);
                            values.put(DBConstants.DB_V2.TablePhones.Fields.PHONE, strPhone);
                            db.insert(DBConstants.DB_V2.TablePhones.TABLE_NAME, null, values);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    cursor.close();
                }
            }
            db.execSQL("DROP TABLE IF EXISTS " + DBConstants.DB_V1.TABLE_NAME);
            Log.d("devcpp", "onUpgrade -> DROP TABLE -> " + DBConstants.DB_V1.TABLE_NAME);
            oldVersion = 2;
        }
    }
}
