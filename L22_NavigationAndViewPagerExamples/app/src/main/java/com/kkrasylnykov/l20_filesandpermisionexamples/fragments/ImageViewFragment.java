package com.kkrasylnykov.l20_filesandpermisionexamples.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

import java.io.File;

public class ImageViewFragment extends Fragment {

    public static final String KEY_PATH = "KEY_PATH";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_view, container, false);
        String strPath = "";

        Bundle bundle = getArguments();
        if (bundle!=null){
            strPath = bundle.getString(KEY_PATH, "");
        }

        if (strPath==null || strPath.isEmpty()){
            getActivity().finish();
        }

        File imgFile = new  File(strPath);

        if(imgFile.exists()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;

            int nScale = options.outWidth / width;
            if (nScale<1){
                nScale = 1;
            }

            options = new BitmapFactory.Options();
            options.inSampleSize = nScale;

            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            ImageView imageView = (ImageView) view.findViewById(R.id.ImageViewFragment);

            imageView.setImageBitmap(bitmap);

            Log.d("devcpp ","bitmap.getWidth -> " + bitmap.getWidth());
        } else {
            getActivity().finish();
        }
        return view;
    }
}
