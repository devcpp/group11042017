package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String KEY_ID = "KEY_ID";

    private EditText mNameEditText;
    private EditText mSNameEditText;
    private EditText mPhoneEditText;
    private EditText mAddressEditText;
    private long nId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mNameEditText = (EditText) findViewById(R.id.nameEditTextEditActivity);
        mSNameEditText = (EditText) findViewById(R.id.snameEditTextEditActivity);
        mPhoneEditText = (EditText) findViewById(R.id.phoneEditTextEditActivity);
        mAddressEditText = (EditText) findViewById(R.id.addrEditTextEditActivity);

        Button btnAdd = (Button) findViewById(R.id.addButtonEditActivity);
        btnAdd.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(KEY_ID,-1);
            }
        }

        if (nId!=-1){
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.removeButtonEditActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = userInfoEngine.getUserById(nId);
            mNameEditText.setText(userInfo.getName());
            mSNameEditText.setText(userInfo.getSName());
            //mPhoneEditText.setText(userInfo.getPhone());
            mAddressEditText.setText(userInfo.getAddress());
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.addButtonEditActivity){
            String strName = mNameEditText.getText().toString().trim();
            String strSName = mSNameEditText.getText().toString().trim();
            String strPhone = mPhoneEditText.getText().toString().trim();
            String strAddress = mAddressEditText.getText().toString().trim();

            if (strName.isEmpty()){
                Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
                return;
            }

            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = new UserInfo(nId, strName, strSName, new ArrayList<PhoneInfo>(), strAddress);

            if (userInfo.getId()!=-1){
                userInfoEngine.update(userInfo);
                finish();
            } else {
                userInfoEngine.insert(userInfo);
            }


            mNameEditText.setText("");
            mNameEditText.requestFocus();
            mSNameEditText.setText("");
            mPhoneEditText.setText("");
            mAddressEditText.setText("");
        } else if (v.getId()==R.id.removeButtonEditActivity) {
            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            userInfoEngine.remove(nId);
            finish();
        }
    }
}
