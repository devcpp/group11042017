package com.kkrasylnykov.l14_savedataexample.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.adapters.RecyclerViewAdapter;
import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, RecyclerViewAdapter.OnClickUserInfoItem {
    private static final int REQUEST_CODE_TC_ACTIVITY = 100;

    //private LinearLayout mContainerLinearLayout;
    //private ListView mListView;
    //private UserInfoAdapter adapter;
    private RecyclerView mRecyclerView;
    private ArrayList<UserInfo> arrData;
    private RecyclerViewAdapter adapter;

    private String strSearch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isShowTC()){
            Intent intent = new Intent(this, TCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC_ACTIVITY);

            /*UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            for (int i=0;i<100; i++){
                ArrayList<PhoneInfo> arrPhone = new ArrayList<PhoneInfo>();
                arrPhone.add(new PhoneInfo(-1, "095" + i));
                arrPhone.add(new PhoneInfo(-1, "096" + i));
                arrPhone.add(new PhoneInfo(-1, "097" + i));
                userInfoEngine.insert(new UserInfo("Kos" + i, "Kras" + i, arrPhone, "Devichia, " + i));
            }*/
        }

        arrData = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewMainActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(arrData);
        adapter.setOnClickUserInfoItemListener(this);
        mRecyclerView.setAdapter(adapter);
//        mListView = (ListView) findViewById(R.id.listViewMainActivity);
//        adapter = new UserInfoAdapter(arrData);
//        mListView.setAdapter(adapter);
//        mListView.setOnItemClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditTextMainActivity);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strSearch = s.toString();
                updateList();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.addItemButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeItemButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.updateItemButtonMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                arrData.clear();
                UserInfoEngine userInfoEngine = new UserInfoEngine(MainActivity.this);
                if (strSearch.isEmpty()){
                    arrData.addAll(userInfoEngine.getAllUsers());
                } else {
                    arrData.addAll(userInfoEngine.getUsersBySearch(strSearch));
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setData(arrData);
                    }
                });
            }
        });
        thread.start();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_TC_ACTIVITY){
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isShowTC()){
                finish();
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getTag()!=null){
            long nId = (long) v.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.KEY_ID, nId);
            startActivity(intent);
        } else if (v.getId() == R.id.addButtonMainActivity){
            Intent intent = new Intent(this, EditActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.removeAllButtonMainActivity){
            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            userInfoEngine.removeAll();
            updateList();
        }
        else if (v.getId() == R.id.addItemButtonMainActivity){
            UserInfo userInfo = new UserInfo("UserAdd", "AddUser", "08765", "");
            arrData.add(1, userInfo);
            adapter.notifyItemInserted(2);
        } else if (v.getId() == R.id.removeItemButtonMainActivity){
            arrData.remove(1);
            adapter.notifyItemRemoved(2);
        } else if (v.getId() == R.id.updateItemButtonMainActivity){
            UserInfo userInfo = arrData.get(1);
            userInfo.setName("TEST UPDATE");
            adapter.notifyItemChanged(2);
        }
    }


    @Override
    public void onClickUserInfoItem(UserInfo userInfo) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, userInfo.getId());
        startActivity(intent);
    }
}
