package com.kkrasylnykov.l14_savedataexample.model.wrappers.networkWrappers;

import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.model.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class UserInfoNetworkWrapper {
    private static final String BASE_URL = "http://xutpuk.pp.ua";

    private static final int REQUEST_OK = 200;

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        String strURL = BASE_URL + "/api/users.json";

        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            connection.connect();

            int response = connection.getResponseCode();
            if (response== REQUEST_OK){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
//                                strResponse += tempStr; - NOT TRUE!!!!!
                            } else {
                                strResponse = stringBuilder.toString();
                                Log.d("devcpp","strResponse -> " + strResponse);
                                break;
                            }
                        }
                        reader.close();
                    }
                    is.close();
                }
                if(strResponse!=null && !strResponse.isEmpty()){
                    JSONArray jsonArray = new JSONArray(strResponse);
                    int nCount = jsonArray.length();
                    for (int i=0; i<nCount; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject!=null){
                            arrResult.add(new UserInfo(jsonObject));
                        }
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrResult;
    }
}
