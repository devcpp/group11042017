package com.kkrasylnykov.l14_savedataexample.model;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class BaseEntity {
    private long mId;

    public BaseEntity(){
        mId = -1;
    }

    public BaseEntity(Cursor cursor){
        this.mId = cursor.getLong(0);
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public abstract ContentValues getContentValues();
}
