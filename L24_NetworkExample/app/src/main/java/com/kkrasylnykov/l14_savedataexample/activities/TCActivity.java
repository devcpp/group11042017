package com.kkrasylnykov.l14_savedataexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

public class TCActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tc);

        findViewById(R.id.btnYesTCActivity).setOnClickListener(this);
        findViewById(R.id.btnNoTCActivity).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        AppSettings appSettings = new AppSettings(this);
        appSettings.setIsShowTC(!(v.getId()==R.id.btnYesTCActivity));

        finish();
    }
}
