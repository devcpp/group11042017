package com.kkrasylnykov.l14_savedataexample.model;


import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class PhoneInfo extends BaseEntity {
    private long mUserId;
    private String mStrPhone;
    private boolean mIsDeleted = false;

    public PhoneInfo(String mStrPhone) {
        this(-1, mStrPhone);
    }

    public PhoneInfo(long mUserId, String mStrPhone) {
        super();
        this.mUserId = mUserId;
        this.mStrPhone = mStrPhone;
    }

    public PhoneInfo(Cursor cursor){
        super(cursor);
        this.mUserId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TablePhones.Fields.USER_ID));
        this.mStrPhone = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TablePhones.Fields.PHONE));
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public String getPhone() {
        return mStrPhone;
    }

    public void setPhone(String strPhone) {
        this.mStrPhone = strPhone;
    }

    public boolean isDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(boolean mIsDeleted) {
        this.mIsDeleted = mIsDeleted;
    }

    @Override
    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.DB_V2.TablePhones.Fields.USER_ID, getUserId());
        values.put(DBConstants.DB_V2.TablePhones.Fields.PHONE, getPhone());
        return values;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null){
            if (obj instanceof PhoneInfo){
                PhoneInfo phoneInfo = (PhoneInfo) obj;
                bResult = this.getId() == phoneInfo.getId();
            } else if (obj instanceof Long){
                long nId = (long) obj;
                bResult = this.getId() == nId;
            } else if (obj instanceof String){
                long nId = Long.parseLong((String) obj);
                bResult = this.getId() == nId;
            }
        }
        return bResult;
    }
}
