package l04_listexample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class L04_ListExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        //ArrayList<Float> arrData = new ArrayList<>();
        LinkedList<Float> arrData = new LinkedList<>();
        while(true){
            System.out.print("Enter num: ");
            Float fVal = scanner.nextFloat();
            if (fVal>0) {
                arrData.add(fVal); // arrData.add(0,fVal);
            } else {
                break;
            }
        }
        
        float fSum = 0;
        for (int i=0; i<arrData.size(); i++){
            fSum += arrData.get(i);
        }
        
        //for (Float fVal: arrData){
        //    fSum += fVal;
        //}
        
        System.out.println("fSum = " + fSum);
        
        //arrData.set(0, fSum);
        
        //arrData.remove(new Float(fSum));
        //arrData.remove(4);
    }
    
}
