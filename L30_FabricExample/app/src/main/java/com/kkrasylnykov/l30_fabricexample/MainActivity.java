package com.kkrasylnykov.l30_fabricexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        findViewById(R.id.err1).setOnClickListener(this);
        findViewById(R.id.err2).setOnClickListener(this);
        findViewById(R.id.err3).setOnClickListener(this);
        findViewById(R.id.err4).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.err1:
                Float f = null;
                f.getClass();
                break;
            case R.id.err2:
                int n=0;
                int b = 1/n;
                break;
            case R.id.err3:
                int[] arr = new int[1];
                arr[1] = 2;
                break;
            case R.id.err4:
                throw new RuntimeException("err4");
        }
    }
}
