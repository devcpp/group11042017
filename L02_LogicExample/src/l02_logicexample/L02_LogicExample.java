package l02_logicexample;

import java.util.Scanner;

public class L02_LogicExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        
        int nResult;
        
        if (a>b && b!=0){
            nResult = (a)%b;
        } else if (b>=a && a!=0) {
            nResult = (b)%a;
        } else {
            nResult = -1;
        }
        
        System.out.println("nResult = " + nResult);
    }
    
}
