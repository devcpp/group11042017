package l05_oopexample;
public class Square {
    private float mSide;
    
    public Square(){
        
    }
    
    public Square(float mSide){
        this.mSide = mSide;
    }
    
    public void setDiahonal(float d){
        mSide = d/(float) Math.sqrt(2);
    }
    
    public float getDiahonal(){
        return mSide * (float)Math.sqrt(2);
    }
    
    public void setS(float s){
        if (s<=0){
            System.out.println("Вы ввели не положительное число.");
            return;
        }
        mSide = (float) Math.sqrt(s);
    }
    
    public float getS(){
        return mSide * mSide;
    }
    
    public void setP(float p){
       mSide =  p/4;
    }
    
    public float getP(){
        return mSide * 4;
    }
    
    public void setSide(float side){
        mSide = side;
    }
    
    public float getSide(){
        return mSide;
    }
    
    
}
