package l05_oopexample;

import java.util.Scanner;

public class L05_OOPExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        Square square = new Square();
        
        System.out.println("Выберете значение которое у Вас есть:");
        System.out.println("1. Длинна стороны");
        System.out.println("2. P");
        System.out.println("3. S");
        System.out.println("4. Диагональ");
        
        int nType = scanner.nextInt();
        switch(nType){
            case 1:
                float side = scanner.nextFloat();
                square.setSide(side);
                break;
            case 2:
                float p = scanner.nextFloat();
                square.setP(p);
                break;
            case 3:
                float s = scanner.nextFloat();
                square.setS(s);
                break;
            case 4:
                float d = scanner.nextFloat();
                square.setDiahonal(d);
                break;
        }
        
        System.out.println("Side = " + square.getSide());
        System.out.println("P = " + square.getP());
        System.out.println("S = " + square.getS());
        System.out.println("getDiahonal = " + square.getDiahonal());
    }
    
}
