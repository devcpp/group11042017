package com.kkrasylnykov.l14_savedataexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class UserInfo {
    private long mId;
    private String mName;
    private String mSName;
    private String mPhone;
    private String mAddress;

    public UserInfo(String mName, String mSName, String mPhone, String mAddress) {
        this(-1, mName, mSName, mPhone, mAddress);
    }

    public UserInfo(long mId, String mName, String mSName, String mPhone, String mAddress) {
        this.mId = mId;
        this.mName = mName;
        this.mSName = mSName;
        this.mPhone = mPhone;
        this.mAddress = mAddress;
    }

    public UserInfo(Cursor cursor) {
        this(cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_ID)),
                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_NAME)),
                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_SNAME)),
                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_PHONE)),
                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_ADDRESS)));
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.FIELD_NAME, getName());
        values.put(DBConstants.FIELD_SNAME, getSName());
        values.put(DBConstants.FIELD_PHONE, getPhone());
        values.put(DBConstants.FIELD_ADDRESS, getAddress());
        return values;
    }
}
