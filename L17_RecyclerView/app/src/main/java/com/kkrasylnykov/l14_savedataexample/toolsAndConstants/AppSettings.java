package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {

    private static final String KEY_BOOLEAN_IS_SHOW_TC = "KEY_BOOLEAN_IS_SHOW_TC";

    private SharedPreferences m_SharedPreferences;

    public AppSettings(Context context){
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setIsShowTC(boolean bIsShowTC){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_SHOW_TC, bIsShowTC);
        editor.commit();
    }

    public boolean isShowTC(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_SHOW_TC, true);
    }
}
