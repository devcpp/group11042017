package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME);
    }

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        strSearch = strSearch + "%";
        String strQuery = DBConstants.FIELD_NAME + " LIKE ? OR "
                + DBConstants.FIELD_SNAME + " LIKE ? OR "
                + DBConstants.FIELD_PHONE + " LIKE ? OR "
                + DBConstants.FIELD_ADDRESS + " LIKE ?";
        String[] arrArg = new String[]{strSearch, strSearch, strSearch, strSearch};
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo result = null;
        SQLiteDatabase db = getReadableDB();
        String strQuery = DBConstants.FIELD_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    result = new UserInfo(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return result;
    }

    public void insert(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        db.insert(DBConstants.TABLE_NAME, null, item.getContentValues());
        db.close();
    }

    public void update(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.FIELD_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        db.update(DBConstants.TABLE_NAME, item.getContentValues(), strQuery, arrArg);
        db.close();
    }

    public void remove(UserInfo item){
        remove(item.getId());
    }

    public void remove(long nId){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.FIELD_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        db.delete(DBConstants.TABLE_NAME, strQuery, arrArg);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();
        db.delete(DBConstants.TABLE_NAME, null, null);
        db.close();
    }
}
