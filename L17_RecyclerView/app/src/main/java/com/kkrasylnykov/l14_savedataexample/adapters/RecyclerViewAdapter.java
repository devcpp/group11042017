package com.kkrasylnykov.l14_savedataexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM_USER_INFO = 0;

    private ArrayList<UserInfo> mArrData;

    public RecyclerViewAdapter(ArrayList<UserInfo> arrData){
        mArrData = arrData;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        if (viewType == TYPE_ITEM_USER_INFO){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
            holder = new UserInfoViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int nType = getItemViewType(position);
        if (nType == TYPE_ITEM_USER_INFO){
            UserInfoViewHolder userInfoViewHolder = (UserInfoViewHolder) holder;
            UserInfo userInfo = mArrData.get(position);
            userInfoViewHolder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
            userInfoViewHolder.phoneTextView.setText(userInfo.getPhone());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM_USER_INFO;
    }

    @Override
    public int getItemCount() {
        return mArrData.size();
    }

    private class UserInfoViewHolder extends RecyclerView.ViewHolder{

        TextView nameTextView;
        TextView phoneTextView;

        public UserInfoViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.textViewFullNameItemUserInfo);
            phoneTextView = (TextView) itemView.findViewById(R.id.textViewPhoneItemUserInfo);

        }
    }
}
