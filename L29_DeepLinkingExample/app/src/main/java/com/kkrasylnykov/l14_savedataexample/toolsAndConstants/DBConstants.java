package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;

public class DBConstants {
    public static final String DB_NAME = "db_notepad";
    public static final int DB_VERSION = 2;

    public static class DB_V1{
        public static final String TABLE_NAME = "_UserInfo";

        public static final String FIELD_ID = "_id";            //long
        public static final String FIELD_NAME = "_name";        //String
        public static final String FIELD_SNAME = "_sname";      //String
        public static final String FIELD_PHONE = "_phone";      //String
        public static final String FIELD_ADDRESS = "_address";  //String
    }

    public static class DB_V2{
        public static class TableUserInfo{
            public static final String TABLE_NAME = "_UserInfo_v2";
            public static class Fields{
                public static final String ID = "_id";              //long
                public static final String SERVER_ID = "_server_id";//long
                public static final String NAME = "_name";          //String
                public static final String SNAME = "_sname";        //String
                public static final String ADDRESS = "_address";
            }
        }

        public static class TablePhones{
            public static final String TABLE_NAME = "_Phones_v2";
            public static class Fields{
                public static final String ID = "_id";            //long
                public static final String USER_ID = "_user_id";  //long
                public static final String PHONE = "_phone";      //String
            }
        }
    }



}
