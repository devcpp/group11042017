package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DBConstants.DB_V2.TableUserInfo.TABLE_NAME);
    }

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        strSearch = strSearch + "%";
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.NAME + " LIKE ? OR "
                + DBConstants.DB_V2.TableUserInfo.Fields.SNAME + " LIKE ? OR "
                + DBConstants.DB_V2.TableUserInfo.Fields.ADDRESS + " LIKE ?";
        String[] arrArg = new String[]{strSearch, strSearch, strSearch};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public long getIdByServerId(long nId){
        long nResult = -1;
        SQLiteDatabase db = getReadableDB();
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.SERVER_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    nResult = new UserInfo(cursor).getId();
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return nResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo result = null;
        SQLiteDatabase db = getReadableDB();
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    result = new UserInfo(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return result;
    }

    public long insert(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        long nUserId = db.insert(getTableName(), null, item.getContentValues());
        db.close();
        return nUserId;
    }

    public void update(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        db.update(getTableName(), item.getContentValues(), strQuery, arrArg);
        db.close();
    }

    public void remove(UserInfo item){
        remove(item.getId());
    }

    public void remove(long nId){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.DB_V2.TableUserInfo.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strQuery, arrArg);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();
        db.delete(getTableName(), null, null);
        db.close();
    }
}
