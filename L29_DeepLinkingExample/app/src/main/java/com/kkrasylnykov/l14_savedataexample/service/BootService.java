package com.kkrasylnykov.l14_savedataexample.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

import org.json.JSONException;

import java.util.ArrayList;

public class BootService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp", "BootService -> onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcpp", "BootService -> onStartCommand");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                AppSettings settings = new AppSettings(BootService.this);
                while (true){
                    try {
                        Log.d("devcpp", "user -> " + settings.getLastUserUpdate());
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
