package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.model.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhonesDBWrapper extends BaseDBWrapper {

    public PhonesDBWrapper(Context context) {
        super(context, DBConstants.DB_V2.TablePhones.TABLE_NAME);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nUserId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.USER_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nUserId)};
        Cursor cursor = db.query(getTableName(), null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try {
                if (cursor.moveToFirst()){
                    do {
                        arrResult.add(new PhoneInfo(cursor));
                    } while (cursor.moveToNext());
                }

            }finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public void insert(PhoneInfo item){
        SQLiteDatabase db = getWritableDB();
        db.insert(getTableName(), null, item.getContentValues());
        db.close();
    }

    public void update(PhoneInfo item){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        db.update(getTableName(), item.getContentValues(), strQuery, arrArg);
        db.close();
    }

    public void remove(long nId){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strQuery, arrArg);
        db.close();
    }

    public void removeByUserId(long nUserId){
        SQLiteDatabase db = getWritableDB();
        String strQuery = DBConstants.DB_V2.TablePhones.Fields.USER_ID + " = ?";
        String[] arrArg = new String[]{Long.toString(nUserId)};
        db.delete(getTableName(), strQuery, arrArg);
        db.close();
    }

    public void remove(PhoneInfo item){
        remove(item.getId());
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();
        db.delete(getTableName(), null, null);
        db.close();
    }
}
