package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.adapters.UserInfoAdapter;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final int REQUEST_CODE_TC_ACTIVITY = 100;

    //private LinearLayout mContainerLinearLayout;
    private ListView mListView;
    private UserInfoAdapter adapter;
    private ArrayList<UserInfo> arrData;

    private String strSearch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        DBHelper dbHelper = new DBHelper(this);

        if (appSettings.isShowTC()){
            Intent intent = new Intent(this, TCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC_ACTIVITY);

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values;
            for (int i=0;i<10000; i++){
                values = new ContentValues();
                values.put(DBConstants.FIELD_NAME, "Kos" + i);
                values.put(DBConstants.FIELD_SNAME, "Kras" + i);
                values.put(DBConstants.FIELD_PHONE, "095" + i);
                values.put(DBConstants.FIELD_ADDRESS, "Devichia, " + i);
                db.insert(DBConstants.TABLE_NAME, null, values);
            }
            db.close();
        }

        arrData = new ArrayList<>();

        mListView = (ListView) findViewById(R.id.listViewMainActivity);
        adapter = new UserInfoAdapter(arrData);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditTextMainActivity);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strSearch = s.toString();
                updateList();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        arrData.clear();
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        if (strSearch.isEmpty()){
            arrData.addAll(userInfoEngine.getAllUsers());
        } else {
            arrData.addAll(userInfoEngine.getUsersBySearch(strSearch));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_TC_ACTIVITY){
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isShowTC()){
                finish();
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getTag()!=null){
            long nId = (long) v.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.KEY_ID, nId);
            startActivity(intent);
        } else if (v.getId() == R.id.addButtonMainActivity){
            Intent intent = new Intent(this, EditActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.removeAllButtonMainActivity){
            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(DBConstants.TABLE_NAME, null, null);
            db.close();
            updateList();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, id);
        startActivity(intent);
    }
}
