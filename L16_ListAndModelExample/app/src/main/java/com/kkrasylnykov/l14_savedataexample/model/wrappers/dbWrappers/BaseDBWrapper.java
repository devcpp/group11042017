package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.engines.BaseEngine;

public abstract class BaseDBWrapper {

    private DBHelper dbHelper;
    private String tableName;

    public BaseDBWrapper(Context context, String tableName){
        dbHelper = new DBHelper(context);
        this.tableName = tableName;
    }

    public String getTableName(){
        return tableName;
    }

    public SQLiteDatabase getReadableDB(){
        return dbHelper.getReadableDatabase();
    }

    public SQLiteDatabase getWritableDB(){
        return dbHelper.getWritableDatabase();
    }
}
