package com.kkrasylnykov.l14_savedataexample.model.engines;

import android.content.Context;

import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {
    public UserInfoEngine(Context mContext) {
        super(mContext);
    }

    public ArrayList<UserInfo> getAllUsers(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getAllUsers();
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getUsersBySearch(strSearch);
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getUserById(nId);
    }

    public void insert(UserInfo item){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.insert(item);
    }

    public void update(UserInfo item){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.update(item);

    }

    public void remove(UserInfo item){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.remove(item);
    }

    public void removeAll(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeAll();
    }
}
