package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;

public class DBConstants {
    public static final String DB_NAME = "db_notepad";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "_UserInfo";

    public static final String FIELD_ID = "_id";            //long
    public static final String FIELD_NAME = "_name";        //String
    public static final String FIELD_SNAME = "_sname";      //String
    public static final String FIELD_PHONE = "_phone";      //String
    public static final String FIELD_ADDRESS = "_address";  //String

}
