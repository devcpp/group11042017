package com.kkrasylnykov.l14_savedataexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.model.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME);
    }

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public ArrayList<UserInfo> getUsersBySearch(String strSearch){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        strSearch = strSearch + "%";
        String strQuery = DBConstants.FIELD_NAME + " LIKE ? OR "
                + DBConstants.FIELD_SNAME + " LIKE ? OR "
                + DBConstants.FIELD_PHONE + " LIKE ? OR "
                + DBConstants.FIELD_ADDRESS + " LIKE ?";
        String[] arrArg = new String[]{strSearch, strSearch, strSearch, strSearch};
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, strQuery, arrArg, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        arrResult.add(new UserInfo(cursor));
                    }while (cursor.moveToNext());

                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        return null;
    }

    public void insert(UserInfo item){

    }

    public void update(UserInfo item){

    }

    public void remove(UserInfo item){

    }

    public void removeAll(){

    }
}
