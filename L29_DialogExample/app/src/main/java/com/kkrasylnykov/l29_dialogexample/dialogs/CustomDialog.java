package com.kkrasylnykov.l29_dialogexample.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Window;

import com.kkrasylnykov.l29_dialogexample.R;

public class CustomDialog extends Dialog {

    public CustomDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

}
