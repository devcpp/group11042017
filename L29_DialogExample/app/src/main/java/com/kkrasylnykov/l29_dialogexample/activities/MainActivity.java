package com.kkrasylnykov.l29_dialogexample.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.kkrasylnykov.l29_dialogexample.R;
import com.kkrasylnykov.l29_dialogexample.dialogs.CustomDialog;
import com.kkrasylnykov.l29_dialogexample.dialogs.CustomFragmentDialog;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DATA_CUR_DIALOG = "DATA_CUR_DIALOG";

    int nCurDialog = -1;
    Dialog curDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState!=null){
            nCurDialog = savedInstanceState.getInt(DATA_CUR_DIALOG);
            if (nCurDialog!=-1){
                View v = new View(this);
                v.setId(nCurDialog);
                onClick(v);
            }


        }

        findViewById(R.id.btnAlertDialog).setOnClickListener(this);
        findViewById(R.id.btnPrograssDialog).setOnClickListener(this);
        findViewById(R.id.btnTimeDialog).setOnClickListener(this);
        findViewById(R.id.btnDateDialog).setOnClickListener(this);
        findViewById(R.id.btnCustomDialog).setOnClickListener(this);
        findViewById(R.id.btnCustomFragmentDialog).setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (curDialog==null || !curDialog.isShowing()){
            nCurDialog = -1;
        }
        outState.putInt(DATA_CUR_DIALOG, nCurDialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAlertDialog:
                nCurDialog = R.id.btnAlertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Hello!!!!");
                builder.setTitle("Warning!!!");
                builder.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("devcpp", "onClick -> PositiveButton");
                    }
                });

                builder.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("devcpp", "onClick -> NegativeButton");
                    }
                });

                builder.setNeutralButton("Neutral", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("devcpp", "onClick -> NeutralButton");
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.d("devcpp", "onCancel -> CancelListener");
                    }
                });
                builder.setCancelable(false);
                curDialog = builder.create();
                curDialog.show();
                break;
            case R.id.btnPrograssDialog:
                nCurDialog = R.id.btnPrograssDialog;
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Подождите, идет загрузка...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                curDialog = progressDialog;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        curDialog.dismiss();
                    }
                });
                thread.start();
                break;
            case R.id.btnTimeDialog:
                nCurDialog = R.id.btnTimeDialog;
                curDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("devcpp", "onCancel -> onTimeSet -> " + hourOfDay + ":" + minute);
                    }
                }, 0, 0, false);
                curDialog.show();
                break;
            case R.id.btnDateDialog:
                nCurDialog = R.id.btnDateDialog;
                Calendar calendar = Calendar.getInstance();
                curDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d("devcpp", "onDateSet -> " + year + "/" + month + "/" + dayOfMonth );
                    }
                }, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                curDialog.show();
                break;
            case R.id.btnCustomDialog:
                nCurDialog = R.id.btnCustomDialog;
                curDialog = new CustomDialog(this);
                curDialog.show();
                break;
            case R.id.btnCustomFragmentDialog:
                CustomFragmentDialog dialog = new CustomFragmentDialog();
                dialog.show(getSupportFragmentManager(), "test");
                break;
        }
    }
}
