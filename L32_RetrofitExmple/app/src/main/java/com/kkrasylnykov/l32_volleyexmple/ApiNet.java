package com.kkrasylnykov.l32_volleyexmple;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiNet {
    @GET("api/users/{serverId}.json")
    Call<UserInfo> getUser(@Path("serverId") String serverId);
}
