package com.kkrasylnykov.l26_serviveexample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.kkrasylnykov.l26_serviveexample.R;
import com.kkrasylnykov.l26_serviveexample.services.ExampleService;

public class LoadActivity extends AppCompatActivity {

    private static final String FILE_NAME = "test.zip";

    TextView stateTextView;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && stateTextView != null){
                Bundle bundle = intent.getExtras();
                if (bundle!=null){
                    stateTextView.setText(bundle.getString(ExampleService.KEY_ACTION_DATA, ""));
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        stateTextView = (TextView) findViewById(R.id.textViewStateAndProgress);

        Intent intent = new Intent(this, ExampleService.class);
        intent.putExtra(ExampleService.KEY_FILE_NAME, FILE_NAME);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(ExampleService.ACTION + FILE_NAME));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
}
