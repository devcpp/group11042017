package com.kkrasylnykov.l26_serviveexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ExampleService extends Service {
    public static final String ACTION = "com.kkrasylnykov.l26_serviveexample.services.ExampleService.";

    public static final String KEY_FILE_NAME = "KEY_FILE_NAME";
    public static final String KEY_ACTION_DATA = "KEY_ACTION_DATA";

    private static ArrayList<String> arrIsLoad = new ArrayList();
    private Thread mThread = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp","ExampleService -> onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null){
            String strFileName = "";
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                strFileName = bundle.getString(KEY_FILE_NAME, "");
            }

            if (strFileName==null || strFileName.isEmpty()){
                stopSelf();
            }

            if (arrIsLoad.contains(strFileName)){
                if (mThread==null || !mThread.isAlive()){
                    sendBroadCast("ExampleService -> FILE IS LOAD", strFileName);
                    stopSelf();
                }
            } else {

                final File file = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOWNLOADS), strFileName);
                if (file.exists()){
                    //TODO NOTIFICATION ACTIVITY FILE IS LOAD
                    Log.d("devcpp","ExampleService -> FILE IS LOAD");
                    sendBroadCast("ExampleService -> FILE IS LOAD", file.getName());
                } else {
                    arrIsLoad.add(strFileName);
                    mThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            InputStream input = null;
                            OutputStream output = null;
                            HttpURLConnection connection = null;
                            try {
                                URL url = new URL("http://p.xutpuk.pp.ua/" + file.getName());
                                connection = (HttpURLConnection) url.openConnection();
                                connection.connect();

                                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                    //TODO NOTIFICATION ACTIVITY NETWORK ERROR!!!!
                                    sendBroadCast("ExampleService -> NETWORK ERROR", file.getName());
                                    Log.d("devcpp","ExampleService -> NETWORK ERROR");
                                }

                                int fileLength = connection.getContentLength();

                                input = connection.getInputStream();
                                output = new FileOutputStream(file.getAbsolutePath());

                                byte data[] = new byte[4096];
                                long total = 0;
                                int count;
                                int nOldProgress = -1;
                                while ((count = input.read(data)) != -1) {
                                    total+=count;
                                    int nProgress = (int) ((float)(total * 100)/ fileLength);
                                    //Log.d("devcpp","ExampleService -> nProgress -> " + nProgress);
                                    if (nOldProgress<nProgress){
                                        //TODO NOTIFICATION ACTIVITY nProgress
                                        nOldProgress = nProgress;
                                        sendBroadCast("ExampleService -> nProgress -> " + nProgress, file.getName());
                                        Log.d("devcpp","ExampleService -> nProgress -> " + nProgress);
                                    }
                                    output.write(data, 0, count);
                                }
                            } catch (FileNotFoundException | MalformedURLException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    if (output != null)
                                        output.close();
                                    if (input != null)
                                        input.close();
                                } catch (IOException ignored) {
                                }

                                if (connection != null)
                                    connection.disconnect();
                            }
                            //TODO NOTIFICATION ACTIVITY LOAD IS COMPLETE!!!!
                            sendBroadCast("ExampleService -> LOAD IS COMPLETE", file.getName());
                            Log.d("devcpp","ExampleService -> LOAD IS COMPLETE");
                            mThread = null;
                            stopSelf();
                        }
                    });
                    mThread.start();
                }
            }

        } else {
            stopSelf();
        }
        return START_STICKY;
    }

    private void sendBroadCast(String strText, String fileName){
        Intent intent = new Intent();
        intent.setAction(ACTION + fileName);
        intent.putExtra(KEY_ACTION_DATA, strText);
        sendBroadcast(intent);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","ExampleService -> onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
