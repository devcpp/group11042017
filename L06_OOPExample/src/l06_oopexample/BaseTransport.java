package l06_oopexample;

public abstract class BaseTransport{
    private String  mTrageMark;
    private String  mMark;
    private float   mPrice;
    private String  mColor;
    private int     mYear;
    private String  mTypeFuel;
    private float   mMaxSpeed;

    public String getTrageMark() {
        return mTrageMark;
    }

    public void setTrageMark(String mTrageMark) {
        this.mTrageMark = mTrageMark;
    }

    public String getMark() {
        return mMark;
    }

    public void setMark(String mMark) {
        this.mMark = mMark;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float mPrice) {
        this.mPrice = mPrice;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String mColor) {
        this.mColor = mColor;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(int mYear) {
        this.mYear = mYear;
    }

    public String getTypeFuel() {
        return mTypeFuel;
    }

    public void setTypeFuel(String mTypeFuel) {
        this.mTypeFuel = mTypeFuel;
    }

    public float getMaxSpeed() {
        return mMaxSpeed;
    }

    public void setMaxSpeed(float mMaxSpeed) {
        this.mMaxSpeed = mMaxSpeed;
    }
    
    public abstract void input();
    public abstract void output();
    public abstract boolean search(String s);
}
