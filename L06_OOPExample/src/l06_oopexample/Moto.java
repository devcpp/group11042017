package l06_oopexample;

import java.util.Scanner;

public class Moto extends BaseTransport{
    
    private String mMotoType;

    public void setMotoType(String mMotoType) {
        this.mMotoType = mMotoType;
    }

    public String getMotoType() {
        return mMotoType;
    }   
    

    @Override
    public void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Trade Mark Moto: ");
        String strTradeMark = scanner.nextLine();
        
        System.out.print("Enter Mark Moto: ");
        String strMark = scanner.nextLine();
                
        System.out.print("Enter Color Moto: ");
        String strColor = scanner.nextLine();
        
        System.out.print("Enter Type Fuel Moto: ");
        String strTypeFuel = scanner.nextLine();
        
        System.out.print("Enter Moto Type: ");
        String strMotoType = scanner.nextLine();
        
        System.out.print("Enter Year Moto: ");
        int nYear = scanner.nextInt();
        
        System.out.print("Enter Price Moto: ");
        float fPrice = scanner.nextFloat();
        
        System.out.print("Enter Max Speed Moto: ");
        float fMaxSpeed = scanner.nextFloat();
        
        setTrageMark(strMark);
        setMark(strMark);
        setColor(strColor);
        setTypeFuel(strTypeFuel);
        setMotoType(strMotoType);
        setYear(nYear);
        setPrice(fPrice);
        setMaxSpeed(fMaxSpeed);
    }

    @Override
    public void output() {
        System.out.println(getTrageMark() + " " + getMark() + 
                " " + getMotoType() + " " + getYear() +
                " " + getTypeFuel() + "  -> " + getPrice());
    }

    @Override
    public boolean search(String s) {
        return getMark().contains(s);
    }
    
}
