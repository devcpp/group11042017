package l06_oopexample;

import java.util.Scanner;

public class Car extends BaseTransport {
    
    private String  mBodyType;
    private int     mWheelCount;

    public String getBodyType() {
        return mBodyType;
    }

    public void setBodyType(String mBodyType) {
        this.mBodyType = mBodyType;
    }

    public int getWheelCount() {
        return mWheelCount;
    }

    public void setWheelCount(int mWheelCount) {
        this.mWheelCount = mWheelCount;
    }
    
    

    @Override
    public void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Trade Mark Car: ");
        String strTradeMark = scanner.nextLine();
        
        System.out.print("Enter Mark Car: ");
        String strMark = scanner.nextLine();
                
        System.out.print("Enter Color Car: ");
        String strColor = scanner.nextLine();
        
        System.out.print("Enter Type Fuel Car: ");
        String strTypeFuel = scanner.nextLine();
        
        System.out.print("Enter Body Type Car: ");
        String strBodyType = scanner.nextLine();
        
        System.out.print("Enter Wheel Count Car: ");
        int nWheelCount = scanner.nextInt();
        
        System.out.print("Enter Year Car: ");
        int nYear = scanner.nextInt();
        
        System.out.print("Enter Price Car: ");
        float fPrice = scanner.nextFloat();
        
        System.out.print("Enter Max Speed Car: ");
        float fMaxSpeed = scanner.nextFloat();
        
        setTrageMark(strTradeMark);
        setMark(strMark);
        setColor(strColor);
        setTypeFuel(strTypeFuel);
        setBodyType(strBodyType);
        setWheelCount(nWheelCount);
        setYear(nYear);
        setPrice(fPrice);
        setMaxSpeed(fMaxSpeed);
    }

    @Override
    public String getTrageMark() {
        return "Car " + super.getTrageMark();
    }
    
    

    @Override
    public void output() {
        System.out.println(getTrageMark() + " " + getMark() + 
                " " + getBodyType() + " " + getColor() + " " + getYear() +
                " " + getTypeFuel() + "  -> " + getPrice());
    }

    @Override
    public boolean search(String s) {
        /*if (getTrageMark().contains(s) || getMark().contains(s) 
                || getBodyType().contains(s)){
            return true;
        }
        return false;*/
        return super.getTrageMark().contains(s) || getMark().contains(s) 
                || getBodyType().contains(s);
    }
    
}
