package l06_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L06_OOPExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        ArrayList<BaseTransport> arrData = new ArrayList<>();
        
        boolean bIsDo = true;
        while (bIsDo) {            
            System.out.println("Menu:");
            System.out.println("1. Add Transport");
            System.out.println("2. Show All");
            System.out.println("3. Remove All");
            System.out.println("4. Search");
            System.out.println("0. Exit");
            
            int nType = scanner.nextInt();
            switch(nType){
                case 0:
                    bIsDo = false;
                    break;
                case 1:
                    System.out.println("Menu:");
                    System.out.println("1. Add Car");
                    System.out.println("2. Add Moto");
                    nType = scanner.nextInt();
                    
                    BaseTransport item;
                    if(nType==1){
                        item = new Car();
                    } else {
                        item = new Moto();
                    }
                    
                    item.input();
                    arrData.add(item);
                    break;
                case 2:
                    for (BaseTransport outItem:arrData){
                        outItem.output();
                        /*if (outItem.getClass().getSimpleName().equals("Car")){
                            NOT TRUE!!!!!!!!
                        }*/
                        
                        if (outItem instanceof Car){
                            System.out.println("Wheel Count " + ((Car)outItem).getWheelCount());
                        }
                        
                    }
                    break;
                case 3:
                    arrData.clear();
                    break;
                case 4:
                    scanner.nextLine();
                    String strSearch = scanner.nextLine();
                    for (BaseTransport outItem:arrData){
                        if (outItem.search(strSearch)){
                            outItem.output();
                        }
                    }
                    break;
                
            }
        }
    }
    
}
