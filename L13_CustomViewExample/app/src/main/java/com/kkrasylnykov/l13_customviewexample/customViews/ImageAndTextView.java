package com.kkrasylnykov.l13_customviewexample.customViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l13_customviewexample.R;

public class ImageAndTextView extends LinearLayout {

    private ImageView imageView;
    private TextView textView;

    public ImageAndTextView(Context context) {
        super(context);
        initView(context);
    }

    public ImageAndTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ImageAndTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ImageAndTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_image_and_text, this, true);
        imageView = (ImageView) findViewById(R.id.ImageView);
        textView = (TextView) findViewById(R.id.TextView);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int nMaxHeight = imageView.getMeasuredHeight();
        int nCurrentHeight = textView.getMeasuredHeight();

        while (nCurrentHeight>nMaxHeight){
            float textSize = textView.getTextSize()-2.5f;
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            Log.d("devcpp","textSize -> " + textSize);

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

            nCurrentHeight = textView.getMeasuredHeight();
        }
    }
}
