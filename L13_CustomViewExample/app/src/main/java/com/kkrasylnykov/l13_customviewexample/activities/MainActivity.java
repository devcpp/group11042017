package com.kkrasylnykov.l13_customviewexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.kkrasylnykov.l13_customviewexample.R;
import com.kkrasylnykov.l13_customviewexample.customViews.ResizeTextView;

public class MainActivity extends AppCompatActivity {

    private ResizeTextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (ResizeTextView) findViewById(R.id.resizeTextView);

        Log.d("devcpp","onCreate -> textView -> " + textView.getMeasuredWidth());
        Log.d("devcpp","onCreate -> textView -> " + textView.getMeasuredHeight());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart -> textView -> " + textView.getMeasuredWidth());
        Log.d("devcpp","onStart -> textView -> " + textView.getMeasuredHeight());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume -> textView -> " + textView.getMeasuredWidth());
        Log.d("devcpp","onResume -> textView -> " + textView.getMeasuredHeight());
    }
}
