package com.kkrasylnykov.l13_customviewexample.customViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

public class ResizeTextView extends TextView {

    public ResizeTextView(Context context) {
        super(context);
    }

    public ResizeTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ResizeTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int nMaxWidth = MeasureSpec.getSize(widthMeasureSpec);

        int nUnWidthMeasureSpec = MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED);
        super.onMeasure(nUnWidthMeasureSpec, heightMeasureSpec);

        int nRealWidth = getMeasuredWidth();
        Log.d("devcpp","nMaxWidth -> " + nMaxWidth);
        Log.d("devcpp","nRealWidth -> " + nRealWidth);

        float fDeltaSize = ((float)nRealWidth)/nMaxWidth;

        if (fDeltaSize==0){
            fDeltaSize = 1;
        }

        float fTextSize = getTextSize()/fDeltaSize;
        setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);

        super.onMeasure(nUnWidthMeasureSpec, heightMeasureSpec);

        nRealWidth = getMeasuredWidth();
        while(nRealWidth>nMaxWidth){
            fTextSize = getTextSize()-1;
            setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
            super.onMeasure(nUnWidthMeasureSpec, heightMeasureSpec);
            nRealWidth = getMeasuredWidth();
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
