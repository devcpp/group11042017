package com.kkrasylnykov.l32_volleyexmple;

import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    private long serverId;
    private String name;
    private String sname;

    public UserInfo(){
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "id: " + getServerId() + " " + getName() + " " + getSname() + "\n";
    }
}
